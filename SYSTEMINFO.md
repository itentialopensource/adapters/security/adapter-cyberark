# Cyberark

Vendor: CyberArk
Homepage: https://www.cyberark.com/

Product: CyberArk
Product Page: https://www.cyberark.com/products/

## Introduction
We classify CyberArk into the Security (SASE) domain as CyberArk provides Security and Trust solutions. 

"CyberArk identity security platform enables secure access for any identity — human or machine — to any resource or environment from anywhere, using any device." 

## Why Integrate
The CyberArk adapter from Itential is used to integrate the Itential Automation Platform (IAP) with CyberArk to offer security and trust information. 

With this adapter you have the ability to perform operations with CyberArk such as:

- Secrets
- Users
- Policy

## Additional Product Documentation
The [CyberArk REST API](https://docs.cyberark.com/conjur-enterprise/latest/en/Content/Developer/lp_REST_API.htm?tocpath=Developer%7CREST%C2%A0APIs%7C_____0)
