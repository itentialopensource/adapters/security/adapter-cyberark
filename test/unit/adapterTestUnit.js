/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cyberark',
      type: 'Cyberark',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Cyberark = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Cyberark Adapter Test', () => {
  describe('Cyberark Class Tests', () => {
    const a = new Cyberark(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cyberark'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cyberark'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Cyberark', pronghornDotJson.export);
          assert.equal('Cyberark', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cyberark', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cyberark'));
          assert.equal('Cyberark', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cyberark', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cyberark', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#extendedAccountOverview - errors', () => {
      it('should have a extendedAccountOverview function', (done) => {
        try {
          assert.equal(true, typeof a.extendedAccountOverview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccounts - errors', () => {
      it('should have a getAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getAccounts(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchType', (done) => {
        try {
          a.getAccounts('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'searchType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getAccounts('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getAccounts('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getAccounts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getAccounts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccount - errors', () => {
      it('should have a addAccount function', (done) => {
        try {
          assert.equal(true, typeof a.addAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountDetails - errors', () => {
      it('should have a getAccountDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccount - errors', () => {
      it('should have a updateAccount function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccount - errors', () => {
      it('should have a deleteAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountActivity - errors', () => {
      it('should have a getAccountActivity function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPendingAccount - errors', () => {
      it('should have a addPendingAccount function', (done) => {
        try {
          assert.equal(true, typeof a.addPendingAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPendingAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addPendingAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#linkanAccount - errors', () => {
      it('should have a linkanAccount function', (done) => {
        try {
          assert.equal(true, typeof a.linkanAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.linkanAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-linkanAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocConnectthroughPSM - errors', () => {
      it('should have a adHocConnectthroughPSM function', (done) => {
        try {
          assert.equal(true, typeof a.adHocConnectthroughPSM === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.adHocConnectthroughPSM(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-adHocConnectthroughPSM', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePasswordImmediately - errors', () => {
      it('should have a changePasswordImmediately function', (done) => {
        try {
          assert.equal(true, typeof a.changePasswordImmediately === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changePasswordImmediately(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changePasswordImmediately', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePasswordintheVaultOnly - errors', () => {
      it('should have a changePasswordintheVaultOnly function', (done) => {
        try {
          assert.equal(true, typeof a.changePasswordintheVaultOnly === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changePasswordintheVaultOnly(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changePasswordintheVaultOnly', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePasswordSetNextPassword - errors', () => {
      it('should have a changePasswordSetNextPassword function', (done) => {
        try {
          assert.equal(true, typeof a.changePasswordSetNextPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changePasswordSetNextPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changePasswordSetNextPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkInanExclusiveAccount - errors', () => {
      it('should have a checkInanExclusiveAccount function', (done) => {
        try {
          assert.equal(true, typeof a.checkInanExclusiveAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectUsingPSM - errors', () => {
      it('should have a connectUsingPSM function', (done) => {
        try {
          assert.equal(true, typeof a.connectUsingPSM === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.connectUsingPSM(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-connectUsingPSM', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJustinTimeAccess - errors', () => {
      it('should have a getJustinTimeAccess function', (done) => {
        try {
          assert.equal(true, typeof a.getJustinTimeAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPasswordValue - errors', () => {
      it('should have a getPasswordValue function', (done) => {
        try {
          assert.equal(true, typeof a.getPasswordValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getPasswordValue(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPasswordValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reconcilePassword - errors', () => {
      it('should have a reconcilePassword function', (done) => {
        try {
          assert.equal(true, typeof a.reconcilePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyPassword - errors', () => {
      it('should have a verifyPassword function', (done) => {
        try {
          assert.equal(true, typeof a.verifyPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePassword - errors', () => {
      it('should have a changePassword function', (done) => {
        try {
          assert.equal(true, typeof a.changePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changePassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveredAccountDetails - errors', () => {
      it('should have a getDiscoveredAccountDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDiscoveredAccountDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveredAccounts - errors', () => {
      it('should have a getDiscoveredAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getDiscoveredAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getDiscoveredAccounts(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDiscoveredAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getDiscoveredAccounts('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDiscoveredAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchType', (done) => {
        try {
          a.getDiscoveredAccounts('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'searchType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDiscoveredAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getDiscoveredAccounts('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDiscoveredAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getDiscoveredAccounts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDiscoveredAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDiscoveredAccountsV108 - errors', () => {
      it('should have a addDiscoveredAccountsV108 function', (done) => {
        try {
          assert.equal(true, typeof a.addDiscoveredAccountsV108 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addDiscoveredAccountsV108(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addDiscoveredAccountsV108', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logonCyberArkAuthentication - errors', () => {
      it('should have a logonCyberArkAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.logonCyberArkAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logonCyberArkAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logonCyberArkAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logonLDAPAuthentication - errors', () => {
      it('should have a logonLDAPAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.logonLDAPAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logonLDAPAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logonLDAPAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logonRADIUSAuthentication - errors', () => {
      it('should have a logonRADIUSAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.logonRADIUSAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logonRADIUSAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logonRADIUSAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logonWindowsAuthentication - errors', () => {
      it('should have a logonWindowsAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.logonWindowsAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logonWindowsAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logonWindowsAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logoff - errors', () => {
      it('should have a logoff function', (done) => {
        try {
          assert.equal(true, typeof a.logoff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSafes - errors', () => {
      it('should have a getAllSafes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSafes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSafe - errors', () => {
      it('should have a addSafe function', (done) => {
        try {
          assert.equal(true, typeof a.addSafe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSafe(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addSafe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafebyPlatformID - errors', () => {
      it('should have a getSafebyPlatformID function', (done) => {
        try {
          assert.equal(true, typeof a.getSafebyPlatformID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeDetails - errors', () => {
      it('should have a getSafeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getSafeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSafe - errors', () => {
      it('should have a deleteSafe function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSafe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeAccountGroups - errors', () => {
      it('should have a getSafeAccountGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getSafeAccountGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeMembers - errors', () => {
      it('should have a getSafeMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getSafeMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSafeMember - errors', () => {
      it('should have a addSafeMember function', (done) => {
        try {
          assert.equal(true, typeof a.addSafeMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSafeMember(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addSafeMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDetails - errors', () => {
      it('should have a getUserDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getUserDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should have a getUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getUsers(null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getUsers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedDetails', (done) => {
        try {
          a.getUsers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extendedDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUser - errors', () => {
      it('should have a addUser function', (done) => {
        try {
          assert.equal(true, typeof a.addUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetUserPassword - errors', () => {
      it('should have a resetUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.resetUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.resetUserPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-resetUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateUser - errors', () => {
      it('should have a activateUser function', (done) => {
        try {
          assert.equal(true, typeof a.activateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should have a getGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getGroups(null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroup - errors', () => {
      it('should have a createGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMembertoGroup - errors', () => {
      it('should have a addMembertoGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addMembertoGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addMembertoGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addMembertoGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroup - errors', () => {
      it('should have a updateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should have a deleteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserfromGroup - errors', () => {
      it('should have a removeUserfromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeUserfromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountDetailsV93 - errors', () => {
      it('should have a getAccountDetailsV93 function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountDetailsV93 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keywords', (done) => {
        try {
          a.getAccountDetailsV93(null, null, (data, error) => {
            try {
              const displayE = 'keywords is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccountDetailsV93', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing safe', (done) => {
        try {
          a.getAccountDetailsV93('fakeparam', null, (data, error) => {
            try {
              const displayE = 'safe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccountDetailsV93', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccountV90 - errors', () => {
      it('should have a addAccountV90 function', (done) => {
        try {
          assert.equal(true, typeof a.addAccountV90 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccountV90(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccountV90', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccountDetailsV95 - errors', () => {
      it('should have a updateAccountDetailsV95 function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccountDetailsV95 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccountDetailsV95(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateAccountDetailsV95', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountV93 - errors', () => {
      it('should have a deleteAccountV93 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccountV93 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountValueV97910 - errors', () => {
      it('should have a getAccountValueV97910 function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountValueV97910 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeCredentialsandSetNextPasswordV10 - errors', () => {
      it('should have a changeCredentialsandSetNextPasswordV10 function', (done) => {
        try {
          assert.equal(true, typeof a.changeCredentialsandSetNextPasswordV10 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeCredentialsandSetNextPasswordV10(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changeCredentialsandSetNextPasswordV10', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeCredentialsintheVaultV10 - errors', () => {
      it('should have a changeCredentialsintheVaultV10 function', (done) => {
        try {
          assert.equal(true, typeof a.changeCredentialsintheVaultV10 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeCredentialsintheVaultV10(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-changeCredentialsintheVaultV10', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyCredentialsV97V995 - errors', () => {
      it('should have a verifyCredentialsV97V995 function', (done) => {
        try {
          assert.equal(true, typeof a.verifyCredentialsV97V995 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.verifyCredentialsV97V995(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-verifyCredentialsV97V995', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logon - errors', () => {
      it('should have a logon function', (done) => {
        try {
          assert.equal(true, typeof a.logon === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logon(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logon', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff - errors', () => {
      it('should have a postLogoff function', (done) => {
        try {
          assert.equal(true, typeof a.postLogoff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSafes1 - errors', () => {
      it('should have a getAllSafes1 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSafes1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAddSafe - errors', () => {
      it('should have a postAddSafe function', (done) => {
        try {
          assert.equal(true, typeof a.postAddSafe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAddSafe(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postAddSafe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeDetails1 - errors', () => {
      it('should have a getSafeDetails1 function', (done) => {
        try {
          assert.equal(true, typeof a.getSafeDetails1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSafe - errors', () => {
      it('should have a updateSafe function', (done) => {
        try {
          assert.equal(true, typeof a.updateSafe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSafe(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateSafe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSafeMembers1 - errors', () => {
      it('should have a getSafeMembers1 function', (done) => {
        try {
          assert.equal(true, typeof a.getSafeMembers1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSafeMember - errors', () => {
      it('should have a updateSafeMember function', (done) => {
        try {
          assert.equal(true, typeof a.updateSafeMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSafeMember(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateSafeMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSafeMember - errors', () => {
      it('should have a deleteSafeMember function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSafeMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDetails1 - errors', () => {
      it('should have a getUserDetails1 function', (done) => {
        try {
          assert.equal(true, typeof a.getUserDetails1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdateUser - errors', () => {
      it('should have a putUpdateUser function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUpdateUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-putUpdateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser1 - errors', () => {
      it('should have a deleteUser1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loggedOnUserDetails - errors', () => {
      it('should have a loggedOnUserDetails function', (done) => {
        try {
          assert.equal(true, typeof a.loggedOnUserDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAddUser - errors', () => {
      it('should have a postAddUser function', (done) => {
        try {
          assert.equal(true, typeof a.postAddUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAddUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postAddUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putActivateUser - errors', () => {
      it('should have a putActivateUser function', (done) => {
        try {
          assert.equal(true, typeof a.putActivateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAddMembertoGroup - errors', () => {
      it('should have a postAddMembertoGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postAddMembertoGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAddMembertoGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postAddMembertoGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountGroupbySafe - errors', () => {
      it('should have a getAccountGroupbySafe function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountGroupbySafe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing safe', (done) => {
        try {
          a.getAccountGroupbySafe(null, (data, error) => {
            try {
              const displayE = 'safe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getAccountGroupbySafe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountGroupMembers - errors', () => {
      it('should have a getAccountGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccountGroup - errors', () => {
      it('should have a addAccountGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addAccountGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccountGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccountGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccounttoAccountGroup - errors', () => {
      it('should have a addAccounttoAccountGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addAccounttoAccountGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccounttoAccountGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccounttoAccountGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMemberfromAccountGroup - errors', () => {
      it('should have a deleteMemberfromAccountGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMemberfromAccountGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteMemberfromAccountGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteMemberfromAccountGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBulkAccountUploadsforUser - errors', () => {
      it('should have a getAllBulkAccountUploadsforUser function', (done) => {
        try {
          assert.equal(true, typeof a.getAllBulkAccountUploadsforUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkUploadofAccounts - errors', () => {
      it('should have a createBulkUploadofAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkUploadofAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkUploadofAccounts(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createBulkUploadofAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkAccountUploadResult - errors', () => {
      it('should have a getBulkAccountUploadResult function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkAccountUploadResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplications - errors', () => {
      it('should have a listApplications function', (done) => {
        try {
          assert.equal(true, typeof a.listApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.listApplications(null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-listApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing includeSublocations', (done) => {
        try {
          a.listApplications('fakeparam', null, (data, error) => {
            try {
              const displayE = 'includeSublocations is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-listApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplication - errors', () => {
      it('should have a addApplication function', (done) => {
        try {
          assert.equal(true, typeof a.addApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteaSpecificApplication - errors', () => {
      it('should have a deleteaSpecificApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteaSpecificApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appID', (done) => {
        try {
          a.deleteaSpecificApplication(null, (data, error) => {
            try {
              const displayE = 'appID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteaSpecificApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallAuthenticationMethodsofaSpecificApplication - errors', () => {
      it('should have a listallAuthenticationMethodsofaSpecificApplication function', (done) => {
        try {
          assert.equal(true, typeof a.listallAuthenticationMethodsofaSpecificApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAuthentication - errors', () => {
      it('should have a addAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.addAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteaSpecificAuthentication - errors', () => {
      it('should have a deleteaSpecificAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteaSpecificAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authID', (done) => {
        try {
          a.deleteaSpecificAuthentication(null, (data, error) => {
            try {
              const displayE = 'authID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteaSpecificAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogon - errors', () => {
      it('should have a postLogon function', (done) => {
        try {
          assert.equal(true, typeof a.postLogon === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing concurrentSession', (done) => {
        try {
          a.postLogon(null, null, null, (data, error) => {
            try {
              const displayE = 'concurrentSession is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postLogon', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apiUse', (done) => {
        try {
          a.postLogon('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'apiUse is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postLogon', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sAMLResponse', (done) => {
        try {
          a.postLogon('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sAMLResponse is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-postLogon', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff1 - errors', () => {
      it('should have a postLogoff1 function', (done) => {
        try {
          assert.equal(true, typeof a.postLogoff1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogon1 - errors', () => {
      it('should have a postLogon1 function', (done) => {
        try {
          assert.equal(true, typeof a.postLogon1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogoff12 - errors', () => {
      it('should have a postLogoff12 function', (done) => {
        try {
          assert.equal(true, typeof a.postLogoff12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpenIDConnectIdentityProviders - errors', () => {
      it('should have a getAllOpenIDConnectIdentityProviders function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOpenIDConnectIdentityProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOpenIDConnectIdentityProvider - errors', () => {
      it('should have a addOpenIDConnectIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.addOpenIDConnectIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOpenIDConnectIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addOpenIDConnectIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecificOpenIDConnectIdentityProvider - errors', () => {
      it('should have a getSpecificOpenIDConnectIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.getSpecificOpenIDConnectIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOpenIDConnectIdentityProvider - errors', () => {
      it('should have a updateOpenIDConnectIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.updateOpenIDConnectIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateOpenIDConnectIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateOpenIDConnectIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpenIDConnectIdentityProvider - errors', () => {
      it('should have a deleteOpenIDConnectIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOpenIDConnectIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationMethods - errors', () => {
      it('should have a getAuthenticationMethods function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationMethods === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAuthenticationMethod - errors', () => {
      it('should have a addAuthenticationMethod function', (done) => {
        try {
          assert.equal(true, typeof a.addAuthenticationMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAuthenticationMethod(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAuthenticationMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecificAuthenticationMethod - errors', () => {
      it('should have a getSpecificAuthenticationMethod function', (done) => {
        try {
          assert.equal(true, typeof a.getSpecificAuthenticationMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthenticationMethod - errors', () => {
      it('should have a updateAuthenticationMethod function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthenticationMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAuthenticationMethod(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateAuthenticationMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticationMethod - errors', () => {
      it('should have a deleteAuthenticationMethod function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticationMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pTAAuthentication - errors', () => {
      it('should have a pTAAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.pTAAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.pTAAuthentication(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-pTAAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.pTAAuthentication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-pTAAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#login - errors', () => {
      it('should have a login function', (done) => {
        try {
          assert.equal(true, typeof a.login === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#whoami - errors', () => {
      it('should have a whoami function', (done) => {
        try {
          assert.equal(true, typeof a.whoami === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticate - errors', () => {
      it('should have a authenticate function', (done) => {
        try {
          assert.equal(true, typeof a.authenticate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeYourPassword - errors', () => {
      it('should have a changeYourPassword function', (done) => {
        try {
          assert.equal(true, typeof a.changeYourPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ePMAuthentication - errors', () => {
      it('should have a ePMAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.ePMAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.ePMAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-ePMAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#windowsAuthentication - errors', () => {
      it('should have a windowsAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.windowsAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.windowsAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-windowsAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logout - errors', () => {
      it('should have a logout function', (done) => {
        try {
          assert.equal(true, typeof a.logout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPassword - errors', () => {
      it('should have a getPassword function', (done) => {
        try {
          assert.equal(true, typeof a.getPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appID', (done) => {
        try {
          a.getPassword(null, null, null, (data, error) => {
            try {
              const displayE = 'appID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing safe', (done) => {
        try {
          a.getPassword('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'safe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing object', (done) => {
        try {
          a.getPassword('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'object is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rotateYourOwnAPIKey - errors', () => {
      it('should have a rotateYourOwnAPIKey function', (done) => {
        try {
          assert.equal(true, typeof a.rotateYourOwnAPIKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#batchRetrieval - errors', () => {
      it('should have a batchRetrieval function', (done) => {
        try {
          assert.equal(true, typeof a.batchRetrieval === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableIds', (done) => {
        try {
          a.batchRetrieval(null, (data, error) => {
            try {
              const displayE = 'variableIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-batchRetrieval', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveASecret - errors', () => {
      it('should have a retrieveASecret function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveASecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addASecret - errors', () => {
      it('should have a addASecret function', (done) => {
        try {
          assert.equal(true, typeof a.addASecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addASecret(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addASecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addKubernetesCACertificate - errors', () => {
      it('should have a addKubernetesCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addKubernetesCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addKubernetesCACertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addKubernetesCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appendtoaPolicy - errors', () => {
      it('should have a appendtoaPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.appendtoaPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.appendtoaPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-appendtoaPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replacePolicy - errors', () => {
      it('should have a replacePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.replacePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replacePolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-replacePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aWSauthnIamPolicyAppendtoRootPolicy - errors', () => {
      it('should have a aWSauthnIamPolicyAppendtoRootPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.aWSauthnIamPolicyAppendtoRootPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.aWSauthnIamPolicyAppendtoRootPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-aWSauthnIamPolicyAppendtoRootPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyAddAuthPolicyModifier - errors', () => {
      it('should have a baseUrlPolicyAddAuthPolicyModifier function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyAddAuthPolicyModifier === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyAddAuthPolicyModifier(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyAddAuthPolicyModifier', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyDeleteAuthPolicyModifier - errors', () => {
      it('should have a baseUrlPolicyDeleteAuthPolicyModifier function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyDeleteAuthPolicyModifier === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyDeleteAuthPolicyModifier(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyDeleteAuthPolicyModifier', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyDeletePolicyBlock - errors', () => {
      it('should have a baseUrlPolicyDeletePolicyBlock function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyDeletePolicyBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyDeletePolicyBlock(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyDeletePolicyBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetAuthPolicyModifiers - errors', () => {
      it('should have a baseUrlPolicyGetAuthPolicyModifiers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetAuthPolicyModifiers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetNicePlinks - errors', () => {
      it('should have a baseUrlPolicyGetNicePlinks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetNicePlinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetNicePolicyBlock - errors', () => {
      it('should have a baseUrlPolicyGetNicePolicyBlock function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetNicePolicyBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlPolicyGetNicePolicyBlock(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetNicePolicyBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetOathOtpClientName - errors', () => {
      it('should have a baseUrlPolicyGetOathOtpClientName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetOathOtpClientName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPasswordComplexityRequirements - errors', () => {
      it('should have a baseUrlPolicyGetPasswordComplexityRequirements function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPasswordComplexityRequirements === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuidOrName', (done) => {
        try {
          a.baseUrlPolicyGetPasswordComplexityRequirements(null, (data, error) => {
            try {
              const displayE = 'uuidOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetPasswordComplexityRequirements', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPlinks - errors', () => {
      it('should have a baseUrlPolicyGetPlinks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPlinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyBlock - errors', () => {
      it('should have a baseUrlPolicyGetPolicyBlock function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPolicyBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlPolicyGetPolicyBlock(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetPolicyBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyBool - errors', () => {
      it('should have a baseUrlPolicyGetPolicyBool function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPolicyBool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyGetPolicyBool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetPolicyBool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyInt - errors', () => {
      it('should have a baseUrlPolicyGetPolicyInt function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPolicyInt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyGetPolicyInt(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetPolicyInt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyMetaData - errors', () => {
      it('should have a baseUrlPolicyGetPolicyMetaData function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPolicyMetaData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetPolicyString - errors', () => {
      it('should have a baseUrlPolicyGetPolicyString function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetPolicyString === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyGetPolicyString(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetPolicyString', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetRsop - errors', () => {
      it('should have a baseUrlPolicyGetRsop function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetRsop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.baseUrlPolicyGetRsop(null, null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetRsop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceid', (done) => {
        try {
          a.baseUrlPolicyGetRsop('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyGetRsop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetU2fClientName - errors', () => {
      it('should have a baseUrlPolicyGetU2fClientName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetU2fClientName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyGetUsingCloudMobileGP - errors', () => {
      it('should have a baseUrlPolicyGetUsingCloudMobileGP function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyGetUsingCloudMobileGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicyPolicyChecks - errors', () => {
      it('should have a baseUrlPolicyPolicyChecks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicyPolicyChecks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicyPolicyChecks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicyPolicyChecks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicySavePolicyBlock2 - errors', () => {
      it('should have a baseUrlPolicySavePolicyBlock2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicySavePolicyBlock2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicySavePolicyBlock2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySavePolicyBlock2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicySavePolicyBlock3 - errors', () => {
      it('should have a baseUrlPolicySavePolicyBlock3 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicySavePolicyBlock3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicySavePolicyBlock3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySavePolicyBlock3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicySetPlinks - errors', () => {
      it('should have a baseUrlPolicySetPlinks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicySetPlinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicySetPlinks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetPlinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicySetPlinksv2 - errors', () => {
      it('should have a baseUrlPolicySetPlinksv2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicySetPlinksv2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlPolicySetPlinksv2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetPlinksv2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPolicySetUsingCloudMobileGP - errors', () => {
      it('should have a baseUrlPolicySetUsingCloudMobileGP function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPolicySetUsingCloudMobileGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing useCloudCA', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'useCloudCA is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hideMobilePolicyForAD', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'hideMobilePolicyForAD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refreshInterval', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'refreshInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gpUpdateInterval', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gpUpdateInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing activeDirectoryCA', (done) => {
        try {
          a.baseUrlPolicySetUsingCloudMobileGP('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'activeDirectoryCA is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPolicySetUsingCloudMobileGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyACL - errors', () => {
      it('should have a listPolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLPolicyID', (done) => {
        try {
          a.listPolicyACL(null, (data, error) => {
            try {
              const displayE = 'aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-listPolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPolicyACL - errors', () => {
      it('should have a addPolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.addPolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPolicyACL(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addPolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLPolicyID', (done) => {
        try {
          a.addPolicyACL('fakeparam', null, (data, error) => {
            try {
              const displayE = 'aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addPolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyACL - errors', () => {
      it('should have a deletePolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicyACL(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deletePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLPolicyID', (done) => {
        try {
          a.deletePolicyACL('fakeparam', null, (data, error) => {
            try {
              const displayE = 'aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deletePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#health - errors', () => {
      it('should have a health function', (done) => {
        try {
          assert.equal(true, typeof a.health === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#information - errors', () => {
      it('should have a information function', (done) => {
        try {
          assert.equal(true, typeof a.information === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicies - errors', () => {
      it('should have a listPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing kind', (done) => {
        try {
          a.listPolicies(null, (data, error) => {
            try {
              const displayE = 'kind is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-listPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxEventsintheLastDay - errors', () => {
      it('should have a inboxEventsintheLastDay function', (done) => {
        try {
          assert.equal(true, typeof a.inboxEventsintheLastDay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventType', (done) => {
        try {
          a.inboxEventsintheLastDay(null, null, null, null, (data, error) => {
            try {
              const displayE = 'eventType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxEventsintheLastDay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publisher', (done) => {
        try {
          a.inboxEventsintheLastDay('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'publisher is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxEventsintheLastDay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing justification', (done) => {
        try {
          a.inboxEventsintheLastDay('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'justification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxEventsintheLastDay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationType', (done) => {
        try {
          a.inboxEventsintheLastDay('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxEventsintheLastDay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxFromSpecificDates - errors', () => {
      it('should have a inboxFromSpecificDates function', (done) => {
        try {
          assert.equal(true, typeof a.inboxFromSpecificDates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publisher', (done) => {
        try {
          a.inboxFromSpecificDates(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'publisher is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateFrom', (done) => {
        try {
          a.inboxFromSpecificDates('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateFrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateTo', (done) => {
        try {
          a.inboxFromSpecificDates('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateTo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing justification', (done) => {
        try {
          a.inboxFromSpecificDates('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'justification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationType', (done) => {
        try {
          a.inboxFromSpecificDates('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.inboxFromSpecificDates('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-inboxFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyAuditFromDatePolicyName - errors', () => {
      it('should have a policyAuditFromDatePolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.policyAuditFromDatePolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publisher', (done) => {
        try {
          a.policyAuditFromDatePolicyName(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'publisher is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateFrom', (done) => {
        try {
          a.policyAuditFromDatePolicyName('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateFrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateTo', (done) => {
        try {
          a.policyAuditFromDatePolicyName('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateTo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing justification', (done) => {
        try {
          a.policyAuditFromDatePolicyName('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'justification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationType', (done) => {
        try {
          a.policyAuditFromDatePolicyName('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyName', (done) => {
        try {
          a.policyAuditFromDatePolicyName('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-policyAuditFromDatePolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#threatDetectionFromSpecificDates - errors', () => {
      it('should have a threatDetectionFromSpecificDates function', (done) => {
        try {
          assert.equal(true, typeof a.threatDetectionFromSpecificDates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publisher', (done) => {
        try {
          a.threatDetectionFromSpecificDates(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'publisher is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-threatDetectionFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateFrom', (done) => {
        try {
          a.threatDetectionFromSpecificDates('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dateFrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-threatDetectionFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dateTo', (done) => {
        try {
          a.threatDetectionFromSpecificDates('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dateTo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-threatDetectionFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing justification', (done) => {
        try {
          a.threatDetectionFromSpecificDates('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'justification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-threatDetectionFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationType', (done) => {
        try {
          a.threatDetectionFromSpecificDates('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-threatDetectionFromSpecificDates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputers - errors', () => {
      it('should have a getComputers function', (done) => {
        try {
          assert.equal(true, typeof a.getComputers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputerGroups - errors', () => {
      it('should have a getComputerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getComputerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should have a getPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicy - errors', () => {
      it('should have a createPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyDetails - errors', () => {
      it('should have a getPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype - errors', () => {
      it('should have a updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updatePolicyForAdHocUserElevationPolicyonlyAdHocElevatePolicytype', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype - errors', () => {
      it('should have a updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updatePolicyForWindowsAdvancedPolicyonlyAdvancedWinAppPolicytype', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRansomwareMode - errors', () => {
      it('should have a updateRansomwareMode function', (done) => {
        try {
          assert.equal(true, typeof a.updateRansomwareMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mode', (done) => {
        try {
          a.updateRansomwareMode(null, (data, error) => {
            try {
              const displayE = 'mode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateRansomwareMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ePMVersion - errors', () => {
      it('should have a ePMVersion function', (done) => {
        try {
          assert.equal(true, typeof a.ePMVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rawEventDetails - errors', () => {
      it('should have a rawEventDetails function', (done) => {
        try {
          assert.equal(true, typeof a.rawEventDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sets - errors', () => {
      it('should have a sets function', (done) => {
        try {
          assert.equal(true, typeof a.sets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclCheckRowRight - errors', () => {
      it('should have a baseUrlAclCheckRowRight function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclCheckRowRight === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclCheckRowRight(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclCheckRowRight', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetAce - errors', () => {
      it('should have a baseUrlAclGetAce function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetAce === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetAce(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetAce', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetCollectionAces - errors', () => {
      it('should have a baseUrlAclGetCollectionAces function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetCollectionAces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetCollectionAces(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetCollectionAces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetCollectionAcesHelper - errors', () => {
      it('should have a baseUrlAclGetCollectionAcesHelper function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetCollectionAcesHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing table', (done) => {
        try {
          a.baseUrlAclGetCollectionAcesHelper(null, null, null, (data, error) => {
            try {
              const displayE = 'table is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetCollectionAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rowkey', (done) => {
        try {
          a.baseUrlAclGetCollectionAcesHelper('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rowkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetCollectionAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reduceSysadmin', (done) => {
        try {
          a.baseUrlAclGetCollectionAcesHelper('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reduceSysadmin is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetCollectionAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetDirAces - errors', () => {
      it('should have a baseUrlAclGetDirAces function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetDirAces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetDirAces(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetDirAces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetEffectiveDirRights - errors', () => {
      it('should have a baseUrlAclGetEffectiveDirRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetEffectiveDirRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetEffectiveDirRights(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetEffectiveDirRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetEffectiveFileRights - errors', () => {
      it('should have a baseUrlAclGetEffectiveFileRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetEffectiveFileRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetEffectiveFileRights(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetEffectiveFileRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetEffectiveRowRights - errors', () => {
      it('should have a baseUrlAclGetEffectiveRowRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetEffectiveRowRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetEffectiveRowRights(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetEffectiveRowRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetFileAces - errors', () => {
      it('should have a baseUrlAclGetFileAces function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetFileAces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetFileAces(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetFileAces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetRowAces - errors', () => {
      it('should have a baseUrlAclGetRowAces function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetRowAces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAclGetRowAces(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetRowAces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAclGetRowAcesHelper - errors', () => {
      it('should have a baseUrlAclGetRowAcesHelper function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAclGetRowAcesHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reduceSys', (done) => {
        try {
          a.baseUrlAclGetRowAcesHelper(null, null, null, null, (data, error) => {
            try {
              const displayE = 'reduceSys is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetRowAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inherit', (done) => {
        try {
          a.baseUrlAclGetRowAcesHelper('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'inherit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetRowAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing table', (done) => {
        try {
          a.baseUrlAclGetRowAcesHelper('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'table is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetRowAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rk', (done) => {
        try {
          a.baseUrlAclGetRowAcesHelper('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'rk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAclGetRowAcesHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAuthProfileDeleteProfile - errors', () => {
      it('should have a baseUrlAuthProfileDeleteProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAuthProfileDeleteProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.baseUrlAuthProfileDeleteProfile(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAuthProfileDeleteProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAuthProfileDeleteProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAuthProfileDeleteProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAuthProfileGetProfile - errors', () => {
      it('should have a baseUrlAuthProfileGetProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAuthProfileGetProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.baseUrlAuthProfileGetProfile(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAuthProfileGetProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAuthProfileGetProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAuthProfileGetProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAuthProfileGetProfileList - errors', () => {
      it('should have a baseUrlAuthProfileGetProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAuthProfileGetProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlAuthProfileSaveProfile - errors', () => {
      it('should have a baseUrlAuthProfileSaveProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlAuthProfileSaveProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlAuthProfileSaveProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlAuthProfileSaveProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlBrandInfo - errors', () => {
      it('should have a baseUrlBrandInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlBrandInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlBrandMyBrand - errors', () => {
      it('should have a baseUrlBrandMyBrand function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlBrandMyBrand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceBulkDeleteUsers - errors', () => {
      it('should have a baseUrlCDirectoryServiceBulkDeleteUsers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceBulkDeleteUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceBulkDeleteUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceBulkDeleteUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceChangeUser - errors', () => {
      it('should have a baseUrlCDirectoryServiceChangeUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceChangeUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceChangeUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceChangeUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceCreateUser - errors', () => {
      it('should have a baseUrlCDirectoryServiceCreateUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceCreateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceCreateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceCreateUserBulk - errors', () => {
      it('should have a baseUrlCDirectoryServiceCreateUserBulk function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceCreateUserBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUserBulk(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceCreateUserBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceCreateUserQuick - errors', () => {
      it('should have a baseUrlCDirectoryServiceCreateUserQuick function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceCreateUserQuick === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUserQuick(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceCreateUserQuick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceCreateUsers - errors', () => {
      it('should have a baseUrlCDirectoryServiceCreateUsers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceCreateUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceCreateUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceCreateUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceDelete - errors', () => {
      it('should have a baseUrlCDirectoryServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlCDirectoryServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceDeleteUser - errors', () => {
      it('should have a baseUrlCDirectoryServiceDeleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceDeleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCDirectoryServiceDeleteUser(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceDeleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceExemptUserFromMfa - errors', () => {
      it('should have a baseUrlCDirectoryServiceExemptUserFromMfa function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceExemptUserFromMfa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCDirectoryServiceExemptUserFromMfa(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceExemptUserFromMfa', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timespan', (done) => {
        try {
          a.baseUrlCDirectoryServiceExemptUserFromMfa('fakeparam', null, (data, error) => {
            try {
              const displayE = 'timespan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceExemptUserFromMfa', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetBulkImportWithExtAtt - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetBulkImportWithExtAtt function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetBulkImportWithExtAtt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetTechSupportUser - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetTechSupportUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetTechSupportUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUser - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceGetUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUserAttributes - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetUserAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetUserAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUserByName - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetUserByName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetUserByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUserByName(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceGetUserByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUsers - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetUsers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGetUsersFromCsvFile - errors', () => {
      it('should have a baseUrlCDirectoryServiceGetUsersFromCsvFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGetUsersFromCsvFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceGetUsersFromCsvFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceGetUsersFromCsvFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceGrantAccess - errors', () => {
      it('should have a baseUrlCDirectoryServiceGrantAccess function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceGrantAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceGrantAccess(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceGrantAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceRefreshToken - errors', () => {
      it('should have a baseUrlCDirectoryServiceRefreshToken function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceRefreshToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCDirectoryServiceRefreshToken(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceRefreshToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlCDirectoryServiceRefreshToken('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceRefreshToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceRemoveAuthSource - errors', () => {
      it('should have a baseUrlCDirectoryServiceRemoveAuthSource function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceRemoveAuthSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceRemoveAuthSource(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceRemoveAuthSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceRemoveFederationAuthSource - errors', () => {
      it('should have a baseUrlCDirectoryServiceRemoveFederationAuthSource function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceRemoveFederationAuthSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing federationUuid', (done) => {
        try {
          a.baseUrlCDirectoryServiceRemoveFederationAuthSource(null, null, (data, error) => {
            try {
              const displayE = 'federationUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceRemoveFederationAuthSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sendInvites', (done) => {
        try {
          a.baseUrlCDirectoryServiceRemoveFederationAuthSource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sendInvites is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceRemoveFederationAuthSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceSetUserPicture - errors', () => {
      it('should have a baseUrlCDirectoryServiceSetUserPicture function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceSetUserPicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCDirectoryServiceSetUserPicture(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceSetUserPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceSetUserPicture('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceSetUserPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceSetUserState - errors', () => {
      it('should have a baseUrlCDirectoryServiceSetUserState function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceSetUserState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceSetUserState(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceSetUserState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck - errors', () => {
      it('should have a baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceStandardJsonResultWithPermissionCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCDirectoryServiceSubmitUploadedFile - errors', () => {
      it('should have a baseUrlCDirectoryServiceSubmitUploadedFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCDirectoryServiceSubmitUploadedFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCDirectoryServiceSubmitUploadedFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCDirectoryServiceSubmitUploadedFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionCreateDynamicCollection - errors', () => {
      it('should have a baseUrlCollectionCreateDynamicCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionCreateDynamicCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionCreateDynamicCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionCreateDynamicCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionCreateManualCollection - errors', () => {
      it('should have a baseUrlCollectionCreateManualCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionCreateManualCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionCreateManualCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionCreateManualCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionDeleteCollection - errors', () => {
      it('should have a baseUrlCollectionDeleteCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionDeleteCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionDeleteCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionDeleteCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetBucketContents - errors', () => {
      it('should have a baseUrlCollectionGetBucketContents function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetBucketContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetBucketContents(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetBucketContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetCollection - errors', () => {
      it('should have a baseUrlCollectionGetCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetCollectionPermissions - errors', () => {
      it('should have a baseUrlCollectionGetCollectionPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetCollectionPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetCollectionPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetCollectionPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetCollectionReferences - errors', () => {
      it('should have a baseUrlCollectionGetCollectionReferences function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetCollectionReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetCollectionReferences(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetCollectionReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetCollectionRights - errors', () => {
      it('should have a baseUrlCollectionGetCollectionRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetCollectionRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetCollectionRights(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetCollectionRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetCollectionTemplate - errors', () => {
      it('should have a baseUrlCollectionGetCollectionTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetCollectionTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetCollectionTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetCollectionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetMembers - errors', () => {
      it('should have a baseUrlCollectionGetMembers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetMembers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetObjectCollections - errors', () => {
      it('should have a baseUrlCollectionGetObjectCollections function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetObjectCollections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetObjectCollections(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetObjectCollections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionGetObjectCollectionsAndFilters - errors', () => {
      it('should have a baseUrlCollectionGetObjectCollectionsAndFilters function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionGetObjectCollectionsAndFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionGetObjectCollectionsAndFilters(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionGetObjectCollectionsAndFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionIsMember - errors', () => {
      it('should have a baseUrlCollectionIsMember function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionIsMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionIsMember(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionIsMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionSetCollectionPermissions - errors', () => {
      it('should have a baseUrlCollectionSetCollectionPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionSetCollectionPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionSetCollectionPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionSetCollectionPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionUpdateCollection - errors', () => {
      it('should have a baseUrlCollectionUpdateCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionUpdateCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionUpdateCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionUpdateCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCollectionUpdateMembersCollection - errors', () => {
      it('should have a baseUrlCollectionUpdateMembersCollection function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCollectionUpdateMembersCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCollectionUpdateMembersCollection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCollectionUpdateMembersCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAddBlockedIpRange - errors', () => {
      it('should have a baseUrlCoreAddBlockedIpRange function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreAddBlockedIpRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlCoreAddBlockedIpRange(null, null, null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddBlockedIpRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oldvalue', (done) => {
        try {
          a.baseUrlCoreAddBlockedIpRange('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'oldvalue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddBlockedIpRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing label', (done) => {
        try {
          a.baseUrlCoreAddBlockedIpRange('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'label is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddBlockedIpRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAddPremDetectRange - errors', () => {
      it('should have a baseUrlCoreAddPremDetectRange function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreAddPremDetectRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlCoreAddPremDetectRange(null, null, null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddPremDetectRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oldvalue', (done) => {
        try {
          a.baseUrlCoreAddPremDetectRange('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'oldvalue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddPremDetectRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing label', (done) => {
        try {
          a.baseUrlCoreAddPremDetectRange('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'label is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAddPremDetectRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAssignDirectoryFileRightsToRoles - errors', () => {
      it('should have a baseUrlCoreAssignDirectoryFileRightsToRoles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreAssignDirectoryFileRightsToRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreAssignDirectoryFileRightsToRoles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAssignDirectoryFileRightsToRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAssignDirectoryRightsToRoles - errors', () => {
      it('should have a baseUrlCoreAssignDirectoryRightsToRoles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreAssignDirectoryRightsToRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreAssignDirectoryRightsToRoles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAssignDirectoryRightsToRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreAssignFileRightsToRoles - errors', () => {
      it('should have a baseUrlCoreAssignFileRightsToRoles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreAssignFileRightsToRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreAssignFileRightsToRoles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreAssignFileRightsToRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCheckProxyHealth - errors', () => {
      it('should have a baseUrlCoreCheckProxyHealth function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreCheckProxyHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreCheckProxyHealth(null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreCheckProxyHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCreateDirectory - errors', () => {
      it('should have a baseUrlCoreCreateDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreCreateDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreCreateDirectory(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreCreateDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCreateTenantReportsDirectory - errors', () => {
      it('should have a baseUrlCoreCreateTenantReportsDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreCreateTenantReportsDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreCreateUserHomeReportsDirectory - errors', () => {
      it('should have a baseUrlCoreCreateUserHomeReportsDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreCreateUserHomeReportsDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDelBlockedIpRange - errors', () => {
      it('should have a baseUrlCoreDelBlockedIpRange function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDelBlockedIpRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlCoreDelBlockedIpRange(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDelBlockedIpRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteAlias - errors', () => {
      it('should have a baseUrlCoreDeleteAlias function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alias', (done) => {
        try {
          a.baseUrlCoreDeleteAlias(null, null, (data, error) => {
            try {
              const displayE = 'alias is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreDeleteAlias('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteAliases - errors', () => {
      it('should have a baseUrlCoreDeleteAliases function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreDeleteAliases(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteAliases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteCertificate - errors', () => {
      it('should have a baseUrlCoreDeleteCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreDeleteCertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteDirectory - errors', () => {
      it('should have a baseUrlCoreDeleteDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreDeleteDirectory(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteFile - errors', () => {
      it('should have a baseUrlCoreDeleteFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreDeleteFile(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteFiles - errors', () => {
      it('should have a baseUrlCoreDeleteFiles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreDeleteFiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteProxies - errors', () => {
      it('should have a baseUrlCoreDeleteProxies function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteProxies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreDeleteProxies(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteProxies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteProxy - errors', () => {
      it('should have a baseUrlCoreDeleteProxy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreDeleteProxy(null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDeleteTenantConfig - errors', () => {
      it('should have a baseUrlCoreDeleteTenantConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDeleteTenantConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.baseUrlCoreDeleteTenantConfig(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDeleteTenantConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDelPremDetectRange - errors', () => {
      it('should have a baseUrlCoreDelPremDetectRange function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDelPremDetectRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlCoreDelPremDetectRange(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDelPremDetectRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDirectoryExists - errors', () => {
      it('should have a baseUrlCoreDirectoryExists function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDirectoryExists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreDirectoryExists(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDirectoryExists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDownloadCertificate - errors', () => {
      it('should have a baseUrlCoreDownloadCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDownloadCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thumbprint', (done) => {
        try {
          a.baseUrlCoreDownloadCertificate(null, null, (data, error) => {
            try {
              const displayE = 'thumbprint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDownloadCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreDownloadCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDownloadCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreDownloadFile - errors', () => {
      it('should have a baseUrlCoreDownloadFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreDownloadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreDownloadFile(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreDownloadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreFileExists - errors', () => {
      it('should have a baseUrlCoreFileExists function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreFileExists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreFileExists(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreFileExists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGenerateNewProxyCode - errors', () => {
      it('should have a baseUrlCoreGenerateNewProxyCode function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGenerateNewProxyCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreGenerateNewProxyCode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGenerateNewProxyCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGeneratePassword - errors', () => {
      it('should have a baseUrlCoreGeneratePassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGeneratePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passwordLength', (done) => {
        try {
          a.baseUrlCoreGeneratePassword(null, (data, error) => {
            try {
              const displayE = 'passwordLength is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGeneratePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetAdLoginSuffixesByForest - errors', () => {
      it('should have a baseUrlCoreGetAdLoginSuffixesByForest function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetAdLoginSuffixesByForest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forestRootDomain', (done) => {
        try {
          a.baseUrlCoreGetAdLoginSuffixesByForest(null, (data, error) => {
            try {
              const displayE = 'forestRootDomain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetAdLoginSuffixesByForest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetAdTopology - errors', () => {
      it('should have a baseUrlCoreGetAdTopology function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetAdTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuidOrDomainName', (done) => {
        try {
          a.baseUrlCoreGetAdTopology(null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuidOrDomainName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetAdTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetAliasesForTenant - errors', () => {
      it('should have a baseUrlCoreGetAliasesForTenant function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetAliasesForTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetAssignedAdministrativeRights - errors', () => {
      it('should have a baseUrlCoreGetAssignedAdministrativeRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetAssignedAdministrativeRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.baseUrlCoreGetAssignedAdministrativeRights(null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetAssignedAdministrativeRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetBlockedIpRanges - errors', () => {
      it('should have a baseUrlCoreGetBlockedIpRanges function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetBlockedIpRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCaCertChain - errors', () => {
      it('should have a baseUrlCoreGetCaCertChain function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetCaCertChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreGetCaCertChain(null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetCaCertChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.baseUrlCoreGetCaCertChain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetCaCertChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCdsAliasesForTenant - errors', () => {
      it('should have a baseUrlCoreGetCdsAliasesForTenant function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetCdsAliasesForTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCloudCACert - errors', () => {
      it('should have a baseUrlCoreGetCloudCACert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetCloudCACert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreGetCloudCACert(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetCloudCACert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetConnectorLog4NetConfig - errors', () => {
      it('should have a baseUrlCoreGetConnectorLog4NetConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetConnectorLog4NetConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyId', (done) => {
        try {
          a.baseUrlCoreGetConnectorLog4NetConfig(null, (data, error) => {
            try {
              const displayE = 'proxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetConnectorLog4NetConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCurrentIwaJsonpUrl - errors', () => {
      it('should have a baseUrlCoreGetCurrentIwaJsonpUrl function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetCurrentIwaJsonpUrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetCurrentIwaUrl - errors', () => {
      it('should have a baseUrlCoreGetCurrentIwaUrl function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetCurrentIwaUrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDefaultGlobalAppSigningCert - errors', () => {
      it('should have a baseUrlCoreGetDefaultGlobalAppSigningCert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDefaultGlobalAppSigningCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certFileName', (done) => {
        try {
          a.baseUrlCoreGetDefaultGlobalAppSigningCert(null, (data, error) => {
            try {
              const displayE = 'certFileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDefaultGlobalAppSigningCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectories - errors', () => {
      it('should have a baseUrlCoreGetDirectories function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetDirectories(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryContents - errors', () => {
      it('should have a baseUrlCoreGetDirectoryContents function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectoryContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetDirectoryContents(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.baseUrlCoreGetDirectoryContents('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileext', (done) => {
        try {
          a.baseUrlCoreGetDirectoryContents('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryFileRolesAndRights - errors', () => {
      it('should have a baseUrlCoreGetDirectoryFileRolesAndRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectoryFileRolesAndRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetDirectoryFileRolesAndRights(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryFileRolesAndRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryInfo - errors', () => {
      it('should have a baseUrlCoreGetDirectoryInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectoryInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetDirectoryInfo(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryRolesAndRights - errors', () => {
      it('should have a baseUrlCoreGetDirectoryRolesAndRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectoryRolesAndRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetDirectoryRolesAndRights(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDirectoryRolesAndRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDirectoryServices - errors', () => {
      it('should have a baseUrlCoreGetDirectoryServices function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDirectoryServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDomainControllersForDomain - errors', () => {
      it('should have a baseUrlCoreGetDomainControllersForDomain function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDomainControllersForDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlCoreGetDomainControllersForDomain(null, null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDomainControllersForDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainName', (done) => {
        try {
          a.baseUrlCoreGetDomainControllersForDomain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetDomainControllersForDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetDownloadUrls - errors', () => {
      it('should have a baseUrlCoreGetDownloadUrls function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetDownloadUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetFileInfo - errors', () => {
      it('should have a baseUrlCoreGetFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetFileInfo(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetFileRolesAndRights - errors', () => {
      it('should have a baseUrlCoreGetFileRolesAndRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetFileRolesAndRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetFileRolesAndRights(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetFileRolesAndRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetIwaTrustRootCert - errors', () => {
      it('should have a baseUrlCoreGetIwaTrustRootCert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetIwaTrustRootCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreGetIwaTrustRootCert(null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetIwaTrustRootCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.baseUrlCoreGetIwaTrustRootCert('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetIwaTrustRootCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetLocTag - errors', () => {
      it('should have a baseUrlCoreGetLocTag function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetLocTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.baseUrlCoreGetLocTag(null, null, null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetLocTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locale', (done) => {
        try {
          a.baseUrlCoreGetLocTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locale is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetLocTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qualifier', (done) => {
        try {
          a.baseUrlCoreGetLocTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qualifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetLocTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetOUTreeContents - errors', () => {
      it('should have a baseUrlCoreGetOUTreeContents function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetOUTreeContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlCoreGetOUTreeContents(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetOUTreeContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlCoreGetOUTreeContents('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetOUTreeContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing useCache', (done) => {
        try {
          a.baseUrlCoreGetOUTreeContents('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'useCache is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetOUTreeContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing getAdminAccountStatus', (done) => {
        try {
          a.baseUrlCoreGetOUTreeContents('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'getAdminAccountStatus is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetOUTreeContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetPremDetectRanges - errors', () => {
      it('should have a baseUrlCoreGetPremDetectRanges function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetPremDetectRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetProxyIwaHostCertificateFile - errors', () => {
      it('should have a baseUrlCoreGetProxyIwaHostCertificateFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetProxyIwaHostCertificateFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreGetProxyIwaHostCertificateFile(null, null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetProxyIwaHostCertificateFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreGetProxyIwaHostCertificateFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetProxyIwaHostCertificateFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetProxyIwaSettings - errors', () => {
      it('should have a baseUrlCoreGetProxyIwaSettings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetProxyIwaSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreGetProxyIwaSettings(null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetPurchasedLicenses - errors', () => {
      it('should have a baseUrlCoreGetPurchasedLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetPurchasedLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetReportsDirectoryContents - errors', () => {
      it('should have a baseUrlCoreGetReportsDirectoryContents function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetReportsDirectoryContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetReportsDirectoryContents(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetReportsDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.baseUrlCoreGetReportsDirectoryContents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetReportsDirectoryContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetSupportedCultures - errors', () => {
      it('should have a baseUrlCoreGetSupportedCultures function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetSupportedCultures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetTenantCACert - errors', () => {
      it('should have a baseUrlCoreGetTenantCACert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetTenantCACert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.baseUrlCoreGetTenantCACert(null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetTenantCACert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.baseUrlCoreGetTenantCACert('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetTenantCACert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetTenantConfig - errors', () => {
      it('should have a baseUrlCoreGetTenantConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetTenantConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.baseUrlCoreGetTenantConfig(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetTenantConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dflt', (done) => {
        try {
          a.baseUrlCoreGetTenantConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dflt is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetTenantConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetUniqueFileName - errors', () => {
      it('should have a baseUrlCoreGetUniqueFileName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetUniqueFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreGetUniqueFileName(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetUniqueFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlCoreGetUniqueFileName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetUniqueFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetUserSettings - errors', () => {
      it('should have a baseUrlCoreGetUserSettings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetUserSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCoreGetUserSettings(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetUserSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing settingType', (done) => {
        try {
          a.baseUrlCoreGetUserSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'settingType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetUserSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetZsoCertAuthority - errors', () => {
      it('should have a baseUrlCoreGetZsoCertAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetZsoCertAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certFileName', (done) => {
        try {
          a.baseUrlCoreGetZsoCertAuthority(null, (data, error) => {
            try {
              const displayE = 'certFileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreGetZsoCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreGetZsoHostInfo - errors', () => {
      it('should have a baseUrlCoreGetZsoHostInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreGetZsoHostInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreHandleTwilioSmsReceipt - errors', () => {
      it('should have a baseUrlCoreHandleTwilioSmsReceipt function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreHandleTwilioSmsReceipt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlCoreHandleTwilioSmsReceipt(null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreHandleTwilioSmsReceipt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreHandleTwilioSmsReceipt('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreHandleTwilioSmsReceipt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreIssueUserCert - errors', () => {
      it('should have a baseUrlCoreIssueUserCert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreIssueUserCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreIssueZsoUserCert - errors', () => {
      it('should have a baseUrlCoreIssueZsoUserCert function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreIssueZsoUserCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certFileName', (done) => {
        try {
          a.baseUrlCoreIssueZsoUserCert(null, null, null, (data, error) => {
            try {
              const displayE = 'certFileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreIssueZsoUserCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certPass', (done) => {
        try {
          a.baseUrlCoreIssueZsoUserCert('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'certPass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreIssueZsoUserCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.baseUrlCoreIssueZsoUserCert('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreIssueZsoUserCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreListDirectory - errors', () => {
      it('should have a baseUrlCoreListDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreListDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreListDirectory(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreListDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreLocalizeReportNameDesc - errors', () => {
      it('should have a baseUrlCoreLocalizeReportNameDesc function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreLocalizeReportNameDesc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreLocalizeReportNameDesc(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreLocalizeReportNameDesc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreMakeFile - errors', () => {
      it('should have a baseUrlCoreMakeFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreMakeFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.baseUrlCoreMakeFile(null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreMakeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing text', (done) => {
        try {
          a.baseUrlCoreMakeFile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'text is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreMakeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contentType', (done) => {
        try {
          a.baseUrlCoreMakeFile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'contentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreMakeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreMoveDirectory - errors', () => {
      it('should have a baseUrlCoreMoveDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreMoveDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreMoveDirectory(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreMoveDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toPath', (done) => {
        try {
          a.baseUrlCoreMoveDirectory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'toPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreMoveDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreNotifyEnvironment - errors', () => {
      it('should have a baseUrlCoreNotifyEnvironment function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreNotifyEnvironment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyId', (done) => {
        try {
          a.baseUrlCoreNotifyEnvironment(null, (data, error) => {
            try {
              const displayE = 'proxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreNotifyEnvironment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreProcessProxyIwaCloudRedirect - errors', () => {
      it('should have a baseUrlCoreProcessProxyIwaCloudRedirect function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreProcessProxyIwaCloudRedirect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oneTimePass', (done) => {
        try {
          a.baseUrlCoreProcessProxyIwaCloudRedirect(null, null, (data, error) => {
            try {
              const displayE = 'oneTimePass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreProcessProxyIwaCloudRedirect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetUrl', (done) => {
        try {
          a.baseUrlCoreProcessProxyIwaCloudRedirect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreProcessProxyIwaCloudRedirect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreReadFile - errors', () => {
      it('should have a baseUrlCoreReadFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreReadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreReadFile(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreReadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreReIssueIwaHostCertificate - errors', () => {
      it('should have a baseUrlCoreReIssueIwaHostCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreReIssueIwaHostCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreReIssueIwaHostCertificate(null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreReIssueIwaHostCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreRenameCertificate - errors', () => {
      it('should have a baseUrlCoreRenameCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreRenameCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thumbprint', (done) => {
        try {
          a.baseUrlCoreRenameCertificate(null, null, (data, error) => {
            try {
              const displayE = 'thumbprint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreRenameCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newName', (done) => {
        try {
          a.baseUrlCoreRenameCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'newName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreRenameCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetConnectorLog4NetConfig - errors', () => {
      it('should have a baseUrlCoreSetConnectorLog4NetConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreSetConnectorLog4NetConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyId', (done) => {
        try {
          a.baseUrlCoreSetConnectorLog4NetConfig(null, null, (data, error) => {
            try {
              const displayE = 'proxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetConnectorLog4NetConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreSetConnectorLog4NetConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetConnectorLog4NetConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetDefaultCertificate - errors', () => {
      it('should have a baseUrlCoreSetDefaultCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreSetDefaultCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.baseUrlCoreSetDefaultCertificate(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetDefaultCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thumbprint', (done) => {
        try {
          a.baseUrlCoreSetDefaultCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'thumbprint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetDefaultCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetProxyIwaHostCertificateFile - errors', () => {
      it('should have a baseUrlCoreSetProxyIwaHostCertificateFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreSetProxyIwaHostCertificateFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaHostCertificateFile(null, null, null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetProxyIwaHostCertificateFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passwd', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaHostCertificateFile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'passwd is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetProxyIwaHostCertificateFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaHostCertificateFile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetProxyIwaHostCertificateFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetProxyIwaSettings - errors', () => {
      it('should have a baseUrlCoreSetProxyIwaSettings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreSetProxyIwaSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaSettings(null, null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreSetProxyIwaSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreSetTenantConfig - errors', () => {
      it('should have a baseUrlCoreSetTenantConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreSetTenantConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.baseUrlCoreSetTenantConfig(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetTenantConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlCoreSetTenantConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreSetTenantConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStartService - errors', () => {
      it('should have a baseUrlCoreStartService function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreStartService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyId', (done) => {
        try {
          a.baseUrlCoreStartService(null, null, (data, error) => {
            try {
              const displayE = 'proxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStartService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.baseUrlCoreStartService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStartService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStopService - errors', () => {
      it('should have a baseUrlCoreStopService function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreStopService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyId', (done) => {
        try {
          a.baseUrlCoreStopService(null, null, (data, error) => {
            try {
              const displayE = 'proxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStopService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.baseUrlCoreStopService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStopService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStoreAlias - errors', () => {
      it('should have a baseUrlCoreStoreAlias function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreStoreAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alias', (done) => {
        try {
          a.baseUrlCoreStoreAlias(null, null, null, null, (data, error) => {
            try {
              const displayE = 'alias is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.baseUrlCoreStoreAlias('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oldName', (done) => {
        try {
          a.baseUrlCoreStoreAlias('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'oldName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdsAlias', (done) => {
        try {
          a.baseUrlCoreStoreAlias('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'cdsAlias is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStoreUser - errors', () => {
      it('should have a baseUrlCoreStoreUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreStoreUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlCoreStoreUser(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.baseUrlCoreStoreUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreStoreUserSetting - errors', () => {
      it('should have a baseUrlCoreStoreUserSetting function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreStoreUserSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlCoreStoreUserSetting(null, null, null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUserSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing target', (done) => {
        try {
          a.baseUrlCoreStoreUserSetting('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'target is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUserSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing settingType', (done) => {
        try {
          a.baseUrlCoreStoreUserSetting('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'settingType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUserSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreStoreUserSetting('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreStoreUserSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreUpdateDirectoryServicesPrecedence - errors', () => {
      it('should have a baseUrlCoreUpdateDirectoryServicesPrecedence function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreUpdateDirectoryServicesPrecedence === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing precedence', (done) => {
        try {
          a.baseUrlCoreUpdateDirectoryServicesPrecedence(null, (data, error) => {
            try {
              const displayE = 'precedence is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreUpdateDirectoryServicesPrecedence', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreUpdateProxyIwaSettings - errors', () => {
      it('should have a baseUrlCoreUpdateProxyIwaSettings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreUpdateProxyIwaSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyUuid', (done) => {
        try {
          a.baseUrlCoreUpdateProxyIwaSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'proxyUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreUpdateProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.baseUrlCoreUpdateProxyIwaSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreUpdateProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreUpdateProxyIwaSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreUpdateProxyIwaSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreUploadCertificate - errors', () => {
      it('should have a baseUrlCoreUploadCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreUploadCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCoreUploadCertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreUploadCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCoreWriteFile - errors', () => {
      it('should have a baseUrlCoreWriteFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCoreWriteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlCoreWriteFile(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreWriteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing text', (done) => {
        try {
          a.baseUrlCoreWriteFile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'text is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreWriteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing content', (done) => {
        try {
          a.baseUrlCoreWriteFile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'content is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCoreWriteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCssIntegrationDzdoExecute - errors', () => {
      it('should have a baseUrlCssIntegrationDzdoExecute function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCssIntegrationDzdoExecute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCssIntegrationDzdoExecute(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCssIntegrationDzdoExecute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCssIntegrationGetChallengeProfiles - errors', () => {
      it('should have a baseUrlCssIntegrationGetChallengeProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCssIntegrationGetChallengeProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCssIntegrationMfaLogin - errors', () => {
      it('should have a baseUrlCssIntegrationMfaLogin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCssIntegrationMfaLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCssIntegrationMfaLogin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCssIntegrationMfaLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlCssIntegrationSetChallengeProfiles - errors', () => {
      it('should have a baseUrlCssIntegrationSetChallengeProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlCssIntegrationSetChallengeProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlCssIntegrationSetChallengeProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlCssIntegrationSetChallengeProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataGetColumn - errors', () => {
      it('should have a baseUrlExtDataGetColumn function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataGetColumn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataGetColumn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataGetColumn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataGetColumns - errors', () => {
      it('should have a baseUrlExtDataGetColumns function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataGetColumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataGetColumns(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataGetColumns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataGetSchema - errors', () => {
      it('should have a baseUrlExtDataGetSchema function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataGetSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataGetSchema(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataGetSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataSetColumn - errors', () => {
      it('should have a baseUrlExtDataSetColumn function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataSetColumn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataSetColumn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataSetColumn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataSetColumns - errors', () => {
      it('should have a baseUrlExtDataSetColumns function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataSetColumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataSetColumns(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataSetColumns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExtDataUpdateSchema - errors', () => {
      it('should have a baseUrlExtDataUpdateSchema function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExtDataUpdateSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExtDataUpdateSchema(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExtDataUpdateSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtAddCertAuthority - errors', () => {
      it('should have a baseUrlExternalCaMgmtAddCertAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExternalCaMgmtAddCertAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExternalCaMgmtAddCertAuthority(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExternalCaMgmtAddCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtDownloadCertAuthority - errors', () => {
      it('should have a baseUrlExternalCaMgmtDownloadCertAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExternalCaMgmtDownloadCertAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing externalCaId', (done) => {
        try {
          a.baseUrlExternalCaMgmtDownloadCertAuthority(null, (data, error) => {
            try {
              const displayE = 'externalCaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExternalCaMgmtDownloadCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtGetCertAuthorities - errors', () => {
      it('should have a baseUrlExternalCaMgmtGetCertAuthorities function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExternalCaMgmtGetCertAuthorities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtRemoveCertAuthority - errors', () => {
      it('should have a baseUrlExternalCaMgmtRemoveCertAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExternalCaMgmtRemoveCertAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing externalCaId', (done) => {
        try {
          a.baseUrlExternalCaMgmtRemoveCertAuthority(null, (data, error) => {
            try {
              const displayE = 'externalCaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExternalCaMgmtRemoveCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlExternalCaMgmtUpdateCertAuthority - errors', () => {
      it('should have a baseUrlExternalCaMgmtUpdateCertAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlExternalCaMgmtUpdateCertAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing externalCaId', (done) => {
        try {
          a.baseUrlExternalCaMgmtUpdateCertAuthority(null, null, (data, error) => {
            try {
              const displayE = 'externalCaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExternalCaMgmtUpdateCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlExternalCaMgmtUpdateCertAuthority('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlExternalCaMgmtUpdateCertAuthority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationAddGlobalGroupAssertionMapping - errors', () => {
      it('should have a baseUrlFederationAddGlobalGroupAssertionMapping function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationAddGlobalGroupAssertionMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationCreateFederation - errors', () => {
      it('should have a baseUrlFederationCreateFederation function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationCreateFederation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationCreateFederation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationCreateFederation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationDeleteFederation - errors', () => {
      it('should have a baseUrlFederationDeleteFederation function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationDeleteFederation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationDeleteFederation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationDeleteFederation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationDeleteGlobalGroupAssertionMapping - errors', () => {
      it('should have a baseUrlFederationDeleteGlobalGroupAssertionMapping function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationDeleteGlobalGroupAssertionMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationFederationMetadata - errors', () => {
      it('should have a baseUrlFederationFederationMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationFederationMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationFederationMetadata(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationFederationMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederatedGroupMembers - errors', () => {
      it('should have a baseUrlFederationGetFederatedGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederatedGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationGetFederatedGroupMembers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationGetFederatedGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederatedGroupsForUser - errors', () => {
      it('should have a baseUrlFederationGetFederatedGroupsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederatedGroupsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationGetFederatedGroupsForUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationGetFederatedGroupsForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederation - errors', () => {
      it('should have a baseUrlFederationGetFederation function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationGetFederation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationGetFederation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederationGroupAssertionMappings - errors', () => {
      it('should have a baseUrlFederationGetFederationGroupAssertionMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederationGroupAssertionMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationGetFederationGroupAssertionMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationGetFederationGroupAssertionMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederations - errors', () => {
      it('should have a baseUrlFederationGetFederations function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetFederationTypes - errors', () => {
      it('should have a baseUrlFederationGetFederationTypes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetFederationTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetGlobalFederationSettings - errors', () => {
      it('should have a baseUrlFederationGetGlobalFederationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetGlobalFederationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationGetGlobalFederationSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationGetGlobalFederationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetGlobalGroupAssertionMappings - errors', () => {
      it('should have a baseUrlFederationGetGlobalGroupAssertionMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetGlobalGroupAssertionMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationGetGroups - errors', () => {
      it('should have a baseUrlFederationGetGroups function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationGetGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationRemoveUserFromFederatedGroup - errors', () => {
      it('should have a baseUrlFederationRemoveUserFromFederatedGroup function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationRemoveUserFromFederatedGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationRemoveUserFromFederatedGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationRemoveUserFromFederatedGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationSPSigningCertificate - errors', () => {
      it('should have a baseUrlFederationSPSigningCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationSPSigningCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationSPSigningCertificateAuthority - errors', () => {
      it('should have a baseUrlFederationSPSigningCertificateAuthority function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationSPSigningCertificateAuthority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationUpdateFederation - errors', () => {
      it('should have a baseUrlFederationUpdateFederation function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationUpdateFederation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationUpdateFederation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationUpdateFederation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationUpdateFederationGroupAssertionMappings - errors', () => {
      it('should have a baseUrlFederationUpdateFederationGroupAssertionMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationUpdateFederationGroupAssertionMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlFederationUpdateFederationGroupAssertionMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlFederationUpdateFederationGroupAssertionMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlFederationUpdateGlobalGroupAssertionMappings - errors', () => {
      it('should have a baseUrlFederationUpdateGlobalGroupAssertionMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlFederationUpdateGlobalGroupAssertionMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryAuthCallback - errors', () => {
      it('should have a baseUrlGoogleDirectoryAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.baseUrlGoogleDirectoryAuthCallback(null, null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlGoogleDirectoryAuthCallback('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing error', (done) => {
        try {
          a.baseUrlGoogleDirectoryAuthCallback('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'error is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryGetAuthTokenState - errors', () => {
      it('should have a baseUrlGoogleDirectoryGetAuthTokenState function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryGetAuthTokenState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pollingToken', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetAuthTokenState(null, (data, error) => {
            try {
              const displayE = 'pollingToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryGetAuthTokenState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryGetDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlGoogleDirectoryGetDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryGetDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryGetDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryGetServiceLoginUrlInfo - errors', () => {
      it('should have a baseUrlGoogleDirectoryGetServiceLoginUrlInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryGetServiceLoginUrlInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlGoogleDirectoryGetServiceLoginUrlInfo(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryGetServiceLoginUrlInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryRemoveDirectoryService - errors', () => {
      it('should have a baseUrlGoogleDirectoryRemoveDirectoryService function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryRemoveDirectoryService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlGoogleDirectoryRemoveDirectoryService(null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryRemoveDirectoryService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlGoogleDirectoryUpdateDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlGoogleDirectoryUpdateDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlGoogleDirectoryUpdateDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlGoogleDirectoryUpdateDirectoryServiceConfig(null, null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryUpdateDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlGoogleDirectoryUpdateDirectoryServiceConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlGoogleDirectoryUpdateDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowDeleteJob - errors', () => {
      it('should have a baseUrlJobFlowDeleteJob function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowDeleteJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowDeleteJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowDeleteJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowEvent - errors', () => {
      it('should have a baseUrlJobFlowEvent function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowEvent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowGetJob - errors', () => {
      it('should have a baseUrlJobFlowGetJob function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowGetJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowGetJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowGetJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowGetJobs - errors', () => {
      it('should have a baseUrlJobFlowGetJobs function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowGetJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowGetJobs(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowGetJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowGetMyJobs - errors', () => {
      it('should have a baseUrlJobFlowGetMyJobs function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowGetMyJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowGetMyJobs(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowGetMyJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJobFlowStartJob - errors', () => {
      it('should have a baseUrlJobFlowStartJob function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJobFlowStartJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlJobFlowStartJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJobFlowStartJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJsManageGetDashboardRolesAndRights - errors', () => {
      it('should have a baseUrlJsManageGetDashboardRolesAndRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJsManageGetDashboardRolesAndRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rowKey', (done) => {
        try {
          a.baseUrlJsManageGetDashboardRolesAndRights(null, (data, error) => {
            try {
              const displayE = 'rowKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJsManageGetDashboardRolesAndRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlJsManageGetReportRolesAndRights - errors', () => {
      it('should have a baseUrlJsManageGetReportRolesAndRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlJsManageGetReportRolesAndRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rowKey', (done) => {
        try {
          a.baseUrlJsManageGetReportRolesAndRights(null, (data, error) => {
            try {
              const displayE = 'rowKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlJsManageGetReportRolesAndRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipConfigure - errors', () => {
      it('should have a baseUrlKmipConfigure function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlKmipConfigure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlKmipConfigure(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlKmipConfigure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipDeleteConfig - errors', () => {
      it('should have a baseUrlKmipDeleteConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlKmipDeleteConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipGetConfig - errors', () => {
      it('should have a baseUrlKmipGetConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlKmipGetConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlKmipPasswordStoredRemotely - errors', () => {
      it('should have a baseUrlKmipPasswordStoredRemotely function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlKmipPasswordStoredRemotely === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceAddLDAPDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceDeleteLDAPDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetCloudConnectors - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetCloudConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetCloudConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetCloudConnectors(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetCloudConnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetDirectoryServiceVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetLDAPDirectoryServiceUuidByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetMappableAttributeList - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetMappableAttributeList function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetMappableAttributeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetMappableAttributeList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetMappableAttributeList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetPropertyToAttributeMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceGetScriptingPropertyToAttributeMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceModifyLDAPDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceSetPropertyToAttributeMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceSetScriptingPropertyToAttributeMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceTestUserLookup - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceTestUserLookup function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceTestUserLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceTestUserLookup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceTestUserLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig - errors', () => {
      it('should have a baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLDAPDirectoryServiceVerifyLDAPDirectoryServiceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileDeleteDevice - errors', () => {
      it('should have a baseUrlMobileDeleteDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileDeleteDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileDeleteDevice(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileDeleteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileDisableSSO - errors', () => {
      it('should have a baseUrlMobileDisableSSO function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileDisableSSO === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileDisableSSO(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileDisableSSO', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileEnableSSO - errors', () => {
      it('should have a baseUrlMobileEnableSSO function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileEnableSSO === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileEnableSSO(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileEnableSSO', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileGetGlobalDevicePermissions - errors', () => {
      it('should have a baseUrlMobileGetGlobalDevicePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileGetGlobalDevicePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlMobileGetGlobalDevicePermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileGetGlobalDevicePermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileKnoxResetContainerPassword - errors', () => {
      it('should have a baseUrlMobileKnoxResetContainerPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileKnoxResetContainerPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileKnoxResetContainerPassword(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileKnoxResetContainerPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileLockClientApp - errors', () => {
      it('should have a baseUrlMobileLockClientApp function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileLockClientApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileLockClientApp(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileLockClientApp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileLockDevice - errors', () => {
      it('should have a baseUrlMobileLockDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileLockDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileLockDevice(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileLockDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobilePingDevice - errors', () => {
      it('should have a baseUrlMobilePingDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobilePingDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobilePingDevice(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobilePingDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobilePowerOff - errors', () => {
      it('should have a baseUrlMobilePowerOff function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobilePowerOff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobilePowerOff(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobilePowerOff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileReapplyDevicePolicy - errors', () => {
      it('should have a baseUrlMobileReapplyDevicePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileReapplyDevicePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileReapplyDevicePolicy(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileReapplyDevicePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileReboot - errors', () => {
      it('should have a baseUrlMobileReboot function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileReboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileReboot(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileReboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileRemoveDeviceProfile - errors', () => {
      it('should have a baseUrlMobileRemoveDeviceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileRemoveDeviceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileRemoveDeviceProfile(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileRemoveDeviceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileResetClientAppLockPin - errors', () => {
      it('should have a baseUrlMobileResetClientAppLockPin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileResetClientAppLockPin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileResetClientAppLockPin(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileResetClientAppLockPin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileSetDevicePermissions - errors', () => {
      it('should have a baseUrlMobileSetDevicePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileSetDevicePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlMobileSetDevicePermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileSetDevicePermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileSetPrimaryDevice - errors', () => {
      it('should have a baseUrlMobileSetPrimaryDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileSetPrimaryDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileSetPrimaryDevice(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileSetPrimaryDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileUnlockDevice - errors', () => {
      it('should have a baseUrlMobileUnlockDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileUnlockDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileUnlockDevice(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileUnlockDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileUpdateDevicePolicy - errors', () => {
      it('should have a baseUrlMobileUpdateDevicePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileUpdateDevicePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileUpdateDevicePolicy(null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileUpdateDevicePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlMobileWipeDevice - errors', () => {
      it('should have a baseUrlMobileWipeDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlMobileWipeDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceID', (done) => {
        try {
          a.baseUrlMobileWipeDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileWipeDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passcode', (done) => {
        try {
          a.baseUrlMobileWipeDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passcode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlMobileWipeDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathAddOrUpdateProfile - errors', () => {
      it('should have a baseUrlOathAddOrUpdateProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathAddOrUpdateProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathAddOrUpdateProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathAddOrUpdateProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathCentrifyOathOtpProfileCheck - errors', () => {
      it('should have a baseUrlOathCentrifyOathOtpProfileCheck function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathCentrifyOathOtpProfileCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userToken', (done) => {
        try {
          a.baseUrlOathCentrifyOathOtpProfileCheck(null, (data, error) => {
            try {
              const displayE = 'userToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathCentrifyOathOtpProfileCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathDeleteProfiles - errors', () => {
      it('should have a baseUrlOathDeleteProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathDeleteProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathDeleteProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathDeleteProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetDataFromCsvFile - errors', () => {
      it('should have a baseUrlOathGetDataFromCsvFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathGetDataFromCsvFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathGetDataFromCsvFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathGetDataFromCsvFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetImportProfileList - errors', () => {
      it('should have a baseUrlOathGetImportProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathGetImportProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetProfileList - errors', () => {
      it('should have a baseUrlOathGetProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathGetProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathGetProfileListForDevice - errors', () => {
      it('should have a baseUrlOathGetProfileListForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathGetProfileListForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathResetCentrifyOathProfile - errors', () => {
      it('should have a baseUrlOathResetCentrifyOathProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathResetCentrifyOathProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathResetCentrifyOathProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathResetCentrifyOathProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathResynchronizeOathToken - errors', () => {
      it('should have a baseUrlOathResynchronizeOathToken function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathResynchronizeOathToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firstCode', (done) => {
        try {
          a.baseUrlOathResynchronizeOathToken(null, null, null, null, (data, error) => {
            try {
              const displayE = 'firstCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathResynchronizeOathToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secondCode', (done) => {
        try {
          a.baseUrlOathResynchronizeOathToken('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'secondCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathResynchronizeOathToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.baseUrlOathResynchronizeOathToken('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathResynchronizeOathToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathResynchronizeOathToken('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathResynchronizeOathToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathSaveProfile - errors', () => {
      it('should have a baseUrlOathSaveProfile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathSaveProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathSaveProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathSaveProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathSetResponseParamsToEntity - errors', () => {
      it('should have a baseUrlOathSetResponseParamsToEntity function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathSetResponseParamsToEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oathProfileDe', (done) => {
        try {
          a.baseUrlOathSetResponseParamsToEntity(null, null, (data, error) => {
            try {
              const displayE = 'oathProfileDe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathSetResponseParamsToEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing showSecret', (done) => {
        try {
          a.baseUrlOathSetResponseParamsToEntity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'showSecret is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathSetResponseParamsToEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathSubmitUploadedFile - errors', () => {
      it('should have a baseUrlOathSubmitUploadedFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathSubmitUploadedFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathSubmitUploadedFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathSubmitUploadedFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathUpdateOathProfileCounter - errors', () => {
      it('should have a baseUrlOathUpdateOathProfileCounter function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathUpdateOathProfileCounter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOathUpdateOathProfileCounter(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathUpdateOathProfileCounter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOathValidateOtpCode - errors', () => {
      it('should have a baseUrlOathValidateOtpCode function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOathValidateOtpCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.baseUrlOathValidateOtpCode(null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathValidateOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing otpCode', (done) => {
        try {
          a.baseUrlOathValidateOtpCode('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'otpCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathValidateOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing useOathDefaults', (done) => {
        try {
          a.baseUrlOathValidateOtpCode('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'useOathDefaults is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOathValidateOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Authorize - errors', () => {
      it('should have a baseUrlOAuth2Authorize function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Authorize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bounce', (done) => {
        try {
          a.baseUrlOAuth2Authorize(null, null, (data, error) => {
            try {
              const displayE = 'bounce is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Authorize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2Authorize('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Authorize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Confirm - errors', () => {
      it('should have a baseUrlOAuth2Confirm function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Confirm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bounce', (done) => {
        try {
          a.baseUrlOAuth2Confirm(null, null, null, (data, error) => {
            try {
              const displayE = 'bounce is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Confirm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing result', (done) => {
        try {
          a.baseUrlOAuth2Confirm('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'result is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Confirm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scopes', (done) => {
        try {
          a.baseUrlOAuth2Confirm('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'scopes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Confirm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2EndSession - errors', () => {
      it('should have a baseUrlOAuth2EndSession function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2EndSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing postLogoutRedirectUri', (done) => {
        try {
          a.baseUrlOAuth2EndSession(null, null, null, (data, error) => {
            try {
              const displayE = 'postLogoutRedirectUri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2EndSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlOAuth2EndSession('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2EndSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idTokenHint', (done) => {
        try {
          a.baseUrlOAuth2EndSession('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'idTokenHint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2EndSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2GetMeta - errors', () => {
      it('should have a baseUrlOAuth2GetMeta function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2GetMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.baseUrlOAuth2GetMeta(null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2GetMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2GetMeta('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2GetMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Introspect - errors', () => {
      it('should have a baseUrlOAuth2Introspect function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Introspect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2Introspect(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Introspect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Keys - errors', () => {
      it('should have a baseUrlOAuth2Keys function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Keys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2Keys(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Keys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Revoke - errors', () => {
      it('should have a baseUrlOAuth2Revoke function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Revoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2Token - errors', () => {
      it('should have a baseUrlOAuth2Token function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2Token === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2Token(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2Token', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2UserInfo - errors', () => {
      it('should have a baseUrlOAuth2UserInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2UserInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2UserInfo(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2UserInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetClients - errors', () => {
      it('should have a baseUrlRadiusGetClients function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusGetClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isQueryResponse', (done) => {
        try {
          a.baseUrlRadiusGetClients(null, (data, error) => {
            try {
              const displayE = 'isQueryResponse is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusGetClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetConfig - errors', () => {
      it('should have a baseUrlRadiusGetConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusGetConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorUuid', (done) => {
        try {
          a.baseUrlRadiusGetConfig(null, (data, error) => {
            try {
              const displayE = 'connectorUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusGetConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetServers - errors', () => {
      it('should have a baseUrlRadiusGetServers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusGetServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusGetUserIdentifierAttributes - errors', () => {
      it('should have a baseUrlRadiusGetUserIdentifierAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusGetUserIdentifierAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusRemoveClients - errors', () => {
      it('should have a baseUrlRadiusRemoveClients function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusRemoveClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRadiusRemoveClients(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusRemoveClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusRemoveServers - errors', () => {
      it('should have a baseUrlRadiusRemoveServers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusRemoveServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRadiusRemoveServers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusRemoveServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusSetClient - errors', () => {
      it('should have a baseUrlRadiusSetClient function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusSetClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRadiusSetClient(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusSetClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusSetConfig - errors', () => {
      it('should have a baseUrlRadiusSetConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusSetConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRadiusSetConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusSetConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRadiusSetServer - errors', () => {
      it('should have a baseUrlRadiusSetServer function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRadiusSetServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRadiusSetServer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRadiusSetServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRegistrationCustomerInfo - errors', () => {
      it('should have a baseUrlRegistrationCustomerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRegistrationCustomerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRegistrationCustomerInfo(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRegistrationCustomerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRegistrationRegisterNewTenant - errors', () => {
      it('should have a baseUrlRegistrationRegisterNewTenant function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRegistrationRegisterNewTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRegistrationRegisterNewTenant(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRegistrationRegisterNewTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRolesGetPagedRoleMembers - errors', () => {
      it('should have a baseUrlRolesGetPagedRoleMembers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRolesGetPagedRoleMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRolesGetPagedRoleMembers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRolesGetPagedRoleMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlRolesUpdateRole - errors', () => {
      it('should have a baseUrlRolesUpdateRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlRolesUpdateRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlRolesUpdateRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlRolesUpdateRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageAddUsersAndGroupsToRole - errors', () => {
      it('should have a baseUrlSaasManageAddUsersAndGroupsToRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageAddUsersAndGroupsToRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageAddUsersAndGroupsToRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageAddUsersAndGroupsToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageDeleteApplication - errors', () => {
      it('should have a baseUrlSaasManageDeleteApplication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageDeleteApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageDeleteApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageDeleteApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageDeleteRole - errors', () => {
      it('should have a baseUrlSaasManageDeleteRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageDeleteRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlSaasManageDeleteRole(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageDeleteRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageDeleteRoles - errors', () => {
      it('should have a baseUrlSaasManageDeleteRoles function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageDeleteRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageDeleteRoles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageDeleteRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetAppIDByServiceName - errors', () => {
      it('should have a baseUrlSaasManageGetAppIDByServiceName function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageGetAppIDByServiceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlSaasManageGetAppIDByServiceName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageGetAppIDByServiceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetApplication - errors', () => {
      it('should have a baseUrlSaasManageGetApplication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageGetApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rowKey', (done) => {
        try {
          a.baseUrlSaasManageGetApplication(null, (data, error) => {
            try {
              const displayE = 'rowKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageGetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetRole - errors', () => {
      it('should have a baseUrlSaasManageGetRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageGetRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlSaasManageGetRole(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageGetRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing suppressPrincipalsList', (done) => {
        try {
          a.baseUrlSaasManageGetRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'suppressPrincipalsList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageGetRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetRoleMembers - errors', () => {
      it('should have a baseUrlSaasManageGetRoleMembers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageGetRoleMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.baseUrlSaasManageGetRoleMembers(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageGetRoleMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageGetTemplatesAndCategories - errors', () => {
      it('should have a baseUrlSaasManageGetTemplatesAndCategories function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageGetTemplatesAndCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageImportAppFromTemplate - errors', () => {
      it('should have a baseUrlSaasManageImportAppFromTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageImportAppFromTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageImportAppFromTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageImportAppFromTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageIsApplicationAvailableInCatalog - errors', () => {
      it('should have a baseUrlSaasManageIsApplicationAvailableInCatalog function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageIsApplicationAvailableInCatalog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appKey', (done) => {
        try {
          a.baseUrlSaasManageIsApplicationAvailableInCatalog(null, (data, error) => {
            try {
              const displayE = 'appKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageIsApplicationAvailableInCatalog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageRemoveUsersAndGroupsFromRole - errors', () => {
      it('should have a baseUrlSaasManageRemoveUsersAndGroupsFromRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageRemoveUsersAndGroupsFromRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageRemoveUsersAndGroupsFromRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageRemoveUsersAndGroupsFromRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageSetApplicationPermissions - errors', () => {
      it('should have a baseUrlSaasManageSetApplicationPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageSetApplicationPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageSetApplicationPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageSetApplicationPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageStoreRole - errors', () => {
      it('should have a baseUrlSaasManageStoreRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageStoreRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageStoreRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageStoreRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageUpdateApplicationDE - errors', () => {
      it('should have a baseUrlSaasManageUpdateApplicationDE function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageUpdateApplicationDE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageUpdateApplicationDE(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageUpdateApplicationDE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSaasManageUpdateRole - errors', () => {
      it('should have a baseUrlSaasManageUpdateRole function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSaasManageUpdateRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSaasManageUpdateRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSaasManageUpdateRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSchedulerHistoryDeleteJobHistory - errors', () => {
      it('should have a baseUrlSchedulerHistoryDeleteJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSchedulerHistoryDeleteJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobid', (done) => {
        try {
          a.baseUrlSchedulerHistoryDeleteJobHistory(null, (data, error) => {
            try {
              const displayE = 'jobid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSchedulerHistoryDeleteJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSchedulerHistoryGetJobReport - errors', () => {
      it('should have a baseUrlSchedulerHistoryGetJobReport function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSchedulerHistoryGetJobReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.baseUrlSchedulerHistoryGetJobReport(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSchedulerHistoryGetJobReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityAdvanceAuthentication - errors', () => {
      it('should have a baseUrlSecurityAdvanceAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityAdvanceAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityAdvanceAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityAdvanceAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityAdvanceForgotUsername - errors', () => {
      it('should have a baseUrlSecurityAdvanceForgotUsername function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityAdvanceForgotUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityAdvanceForgotUsername(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityAdvanceForgotUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityAmIAuthenticated - errors', () => {
      it('should have a baseUrlSecurityAmIAuthenticated function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityAmIAuthenticated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityAmIAuthenticated(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityAmIAuthenticated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityAnswerOOBChallenge - errors', () => {
      it('should have a baseUrlSecurityAnswerOOBChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityAnswerOOBChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing answer', (done) => {
        try {
          a.baseUrlSecurityAnswerOOBChallenge(null, null, (data, error) => {
            try {
              const displayE = 'answer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityAnswerOOBChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityAnswerOOBChallenge('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityAnswerOOBChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityChallengeUser - errors', () => {
      it('should have a baseUrlSecurityChallengeUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityChallengeUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.baseUrlSecurityChallengeUser(null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityChallengeUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityCleanupAuthentication - errors', () => {
      it('should have a baseUrlSecurityCleanupAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityCleanupAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityCleanupAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityCleanupAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityDoIHaveRight - errors', () => {
      it('should have a baseUrlSecurityDoIHaveRight function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityDoIHaveRight === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityForgotUsername - errors', () => {
      it('should have a baseUrlSecurityForgotUsername function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityForgotUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityForgotUsername(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityForgotUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityGetOneTimePassword - errors', () => {
      it('should have a baseUrlSecurityGetOneTimePassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityGetOneTimePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing use', (done) => {
        try {
          a.baseUrlSecurityGetOneTimePassword(null, (data, error) => {
            try {
              const displayE = 'use is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityGetOneTimePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityGetRiskAnalysisLevels - errors', () => {
      it('should have a baseUrlSecurityGetRiskAnalysisLevels function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityGetRiskAnalysisLevels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityLogin - errors', () => {
      it('should have a baseUrlSecurityLogin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing systemID', (done) => {
        try {
          a.baseUrlSecurityLogin(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'systemID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlSecurityLogin('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.baseUrlSecurityLogin('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing persist', (done) => {
        try {
          a.baseUrlSecurityLogin('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'persist is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityLogin('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityLogout - errors', () => {
      it('should have a baseUrlSecurityLogout function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityLogout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing redirectUrl', (done) => {
        try {
          a.baseUrlSecurityLogout(null, null, null, (data, error) => {
            try {
              const displayE = 'redirectUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing allowIWA', (done) => {
        try {
          a.baseUrlSecurityLogout('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'allowIWA is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityLogout('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityLogout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityMultiAuthLogin - errors', () => {
      it('should have a baseUrlSecurityMultiAuthLogin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityMultiAuthLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityMultiAuthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityMultiAuthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing persist', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'persist is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityMultiAuthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing elevate', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'elevate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityMultiAuthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityMultiAuthLogin('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityMultiAuthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityOnDemandChallenge - errors', () => {
      it('should have a baseUrlSecurityOnDemandChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityOnDemandChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityOnDemandChallenge(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityOnDemandChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityRefreshToken - errors', () => {
      it('should have a baseUrlSecurityRefreshToken function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityRefreshToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityResumeFromExtIdpAuth - errors', () => {
      it('should have a baseUrlSecurityResumeFromExtIdpAuth function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityResumeFromExtIdpAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extIdpAuthChallengeState', (done) => {
        try {
          a.baseUrlSecurityResumeFromExtIdpAuth(null, null, (data, error) => {
            try {
              const displayE = 'extIdpAuthChallengeState is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityResumeFromExtIdpAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityResumeFromExtIdpAuth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityResumeFromExtIdpAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityStartAuthentication - errors', () => {
      it('should have a baseUrlSecurityStartAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityStartAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityStartAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityStartAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityStartChallenge - errors', () => {
      it('should have a baseUrlSecurityStartChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityStartChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityStartChallenge(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityStartChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityStartForgotUsername - errors', () => {
      it('should have a baseUrlSecurityStartForgotUsername function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityStartForgotUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityStartForgotUsername(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityStartForgotUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityStartSocialAuthentication - errors', () => {
      it('should have a baseUrlSecurityStartSocialAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityStartSocialAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityStartSocialAuthentication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityStartSocialAuthentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecuritySubmitOathOtpCode - errors', () => {
      it('should have a baseUrlSecuritySubmitOathOtpCode function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecuritySubmitOathOtpCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing otpCode', (done) => {
        try {
          a.baseUrlSecuritySubmitOathOtpCode(null, null, null, (data, error) => {
            try {
              const displayE = 'otpCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecuritySubmitOathOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userUuid', (done) => {
        try {
          a.baseUrlSecuritySubmitOathOtpCode('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecuritySubmitOathOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecuritySubmitOathOtpCode('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecuritySubmitOathOtpCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityTaskCheck - errors', () => {
      it('should have a baseUrlSecurityTaskCheck function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityTaskCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing task', (done) => {
        try {
          a.baseUrlSecurityTaskCheck(null, (data, error) => {
            try {
              const displayE = 'task is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityTaskCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityTaskChecks - errors', () => {
      it('should have a baseUrlSecurityTaskChecks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityTaskChecks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityTaskChecks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityTaskChecks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityTwilioPhoneChallengeCompleted - errors', () => {
      it('should have a baseUrlSecurityTwilioPhoneChallengeCompleted function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityTwilioPhoneChallengeCompleted === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityTwilioPhoneChallengeCompleted(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityTwilioPhoneChallengeCompleted', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityTwilioPhoneChallengeNotAnswered - errors', () => {
      it('should have a baseUrlSecurityTwilioPhoneChallengeNotAnswered function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityTwilioPhoneChallengeNotAnswered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSecurityTwilioPhoneChallengeNotAnswered(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityTwilioPhoneChallengeNotAnswered', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSecurityWhoAmI - errors', () => {
      it('should have a baseUrlSecurityWhoAmI function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSecurityWhoAmI === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing challenge', (done) => {
        try {
          a.baseUrlSecurityWhoAmI(null, (data, error) => {
            try {
              const displayE = 'challenge is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSecurityWhoAmI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentAddEnrollmentCode - errors', () => {
      it('should have a baseUrlServerAgentAddEnrollmentCode function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentAddEnrollmentCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentAddEnrollmentCode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentAddEnrollmentCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentDeleteEnrollmentCode - errors', () => {
      it('should have a baseUrlServerAgentDeleteEnrollmentCode function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentDeleteEnrollmentCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentDeleteEnrollmentCode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentDeleteEnrollmentCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentDisableFeatures - errors', () => {
      it('should have a baseUrlServerAgentDisableFeatures function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentDisableFeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentDisableFeatures(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentDisableFeatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnableFeatures - errors', () => {
      it('should have a baseUrlServerAgentEnableFeatures function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentEnableFeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnableFeaturesV2 - errors', () => {
      it('should have a baseUrlServerAgentEnableFeaturesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentEnableFeaturesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentEnableFeaturesV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentEnableFeaturesV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnroll - errors', () => {
      it('should have a baseUrlServerAgentEnroll function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentEnroll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentEnrollV2 - errors', () => {
      it('should have a baseUrlServerAgentEnrollV2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentEnrollV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentEnrollV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentEnrollV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentGetAllEnrollmentCodes - errors', () => {
      it('should have a baseUrlServerAgentGetAllEnrollmentCodes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentGetAllEnrollmentCodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentGetAllEnrollmentCodes(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentGetAllEnrollmentCodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentGetCertificate - errors', () => {
      it('should have a baseUrlServerAgentGetCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentGetCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentGetCertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentGetCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentRegister - errors', () => {
      it('should have a baseUrlServerAgentRegister function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentRegisterV2 - errors', () => {
      it('should have a baseUrlServerAgentRegisterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentRegisterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentRegisterV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentRegisterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentUnenroll - errors', () => {
      it('should have a baseUrlServerAgentUnenroll function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentUnenroll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentUnenroll(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentUnenroll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerAgentVerifyPasswordV2 - errors', () => {
      it('should have a baseUrlServerAgentVerifyPasswordV2 function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerAgentVerifyPasswordV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerAgentVerifyPasswordV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerAgentVerifyPasswordV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageAddAccount - errors', () => {
      it('should have a baseUrlServerManageAddAccount function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageAddAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageAddAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageAddAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageCheckinPassword - errors', () => {
      it('should have a baseUrlServerManageCheckinPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageCheckinPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageCheckinPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageCheckinPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageCheckoutPassword - errors', () => {
      it('should have a baseUrlServerManageCheckoutPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageCheckoutPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageCheckoutPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageCheckoutPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageDeleteAccount - errors', () => {
      it('should have a baseUrlServerManageDeleteAccount function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageDeleteAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageDeleteAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageDeleteAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageExtendCheckout - errors', () => {
      it('should have a baseUrlServerManageExtendCheckout function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageExtendCheckout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageExtendCheckout(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageExtendCheckout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageGetAccountPermissions - errors', () => {
      it('should have a baseUrlServerManageGetAccountPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageGetAccountPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageGetAccountPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageGetAccountPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageGetRetiredPassword - errors', () => {
      it('should have a baseUrlServerManageGetRetiredPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageGetRetiredPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageGetRetiredPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageGetRetiredPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManagePreCheckout - errors', () => {
      it('should have a baseUrlServerManagePreCheckout function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManagePreCheckout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManagePreCheckout(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManagePreCheckout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageSetAccountPermissions - errors', () => {
      it('should have a baseUrlServerManageSetAccountPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageSetAccountPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageSetAccountPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageSetAccountPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageSetDomainPermissions - errors', () => {
      it('should have a baseUrlServerManageSetDomainPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageSetDomainPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageSetDomainPermissions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageSetDomainPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageUpdateAccount - errors', () => {
      it('should have a baseUrlServerManageUpdateAccount function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageUpdateAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageUpdateAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageUpdateAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlServerManageUpdatePassword - errors', () => {
      it('should have a baseUrlServerManageUpdatePassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlServerManageUpdatePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlServerManageUpdatePassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlServerManageUpdatePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthFacebookAuthCallback - errors', () => {
      it('should have a baseUrlSocialAuthFacebookAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthFacebookAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.baseUrlSocialAuthFacebookAuthCallback(null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthFacebookAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlSocialAuthFacebookAuthCallback('fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthFacebookAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthGoogleAuthCallback - errors', () => {
      it('should have a baseUrlSocialAuthGoogleAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthGoogleAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.baseUrlSocialAuthGoogleAuthCallback(null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthGoogleAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlSocialAuthGoogleAuthCallback('fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthGoogleAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthLinkedInAuthCallback - errors', () => {
      it('should have a baseUrlSocialAuthLinkedInAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthLinkedInAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.baseUrlSocialAuthLinkedInAuthCallback(null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthLinkedInAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlSocialAuthLinkedInAuthCallback('fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthLinkedInAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMicrosoftActAuthCallback - errors', () => {
      it('should have a baseUrlSocialAuthMicrosoftActAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMicrosoftActAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.baseUrlSocialAuthMicrosoftActAuthCallback(null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMicrosoftActAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlSocialAuthMicrosoftActAuthCallback('fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMicrosoftActAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthTwitterAuthCallback - errors', () => {
      it('should have a baseUrlSocialAuthTwitterAuthCallback function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthTwitterAuthCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oauthToken', (done) => {
        try {
          a.baseUrlSocialAuthTwitterAuthCallback(null, null, null, (data, error) => {
            try {
              const displayE = 'oauthToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthTwitterAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oauthVerifier', (done) => {
        try {
          a.baseUrlSocialAuthTwitterAuthCallback('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'oauthVerifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthTwitterAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.baseUrlSocialAuthTwitterAuthCallback('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthTwitterAuthCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtGetAllCustomConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtGetAllCustomConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtGetAllCustomConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetAllCustomConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMgmtGetAllCustomConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtGetApplicationClientSecret - errors', () => {
      it('should have a baseUrlSocialAuthMgmtGetApplicationClientSecret function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtGetApplicationClientSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetApplicationClientSecret(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMgmtGetApplicationClientSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtGetAuthConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtGetAuthConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtGetAuthConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtGetCustomConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtGetCustomConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtGetCustomConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSocialAuthMgmtGetCustomConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMgmtGetCustomConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtResetAuthConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtResetAuthConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtResetAuthConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtSetAuthConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtSetAuthConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtSetAuthConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSocialAuthMgmtSetAuthConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMgmtSetAuthConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSocialAuthMgmtSetCustomConfig - errors', () => {
      it('should have a baseUrlSocialAuthMgmtSetCustomConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSocialAuthMgmtSetCustomConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlSocialAuthMgmtSetCustomConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlSocialAuthMgmtSetCustomConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoAbout - errors', () => {
      it('should have a baseUrlSysInfoAbout function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSysInfoAbout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoDummy - errors', () => {
      it('should have a baseUrlSysInfoDummy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSysInfoDummy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoGetMySession - errors', () => {
      it('should have a baseUrlSysInfoGetMySession function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSysInfoGetMySession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlSysInfoVersion - errors', () => {
      it('should have a baseUrlSysInfoVersion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlSysInfoVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskCancelJob - errors', () => {
      it('should have a baseUrlTaskCancelJob function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskCancelJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTaskCancelJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskCancelJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskCreateOneTimeJob - errors', () => {
      it('should have a baseUrlTaskCreateOneTimeJob function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskCreateOneTimeJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTaskCreateOneTimeJob(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskCreateOneTimeJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskEmailReport - errors', () => {
      it('should have a baseUrlTaskEmailReport function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskEmailReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTaskEmailReport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskEmailReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskGetJobHistory - errors', () => {
      it('should have a baseUrlTaskGetJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskGetJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTaskGetJobHistory(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskGetJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskGetSingleJobHistory - errors', () => {
      it('should have a baseUrlTaskGetSingleJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskGetSingleJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.baseUrlTaskGetSingleJobHistory(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskGetSingleJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTaskJobReport - errors', () => {
      it('should have a baseUrlTaskJobReport function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTaskJobReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hoursBack', (done) => {
        try {
          a.baseUrlTaskJobReport(null, (data, error) => {
            try {
              const displayE = 'hoursBack is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTaskJobReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesGet - errors', () => {
      it('should have a baseUrlTenantCnamesGet function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesGetDomainInfo - errors', () => {
      it('should have a baseUrlTenantCnamesGetDomainInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesGetDomainInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesRegister - errors', () => {
      it('should have a baseUrlTenantCnamesRegister function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cnamePrefix', (done) => {
        try {
          a.baseUrlTenantCnamesRegister(null, (data, error) => {
            try {
              const displayE = 'cnamePrefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantCnamesRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesSetPreferred - errors', () => {
      it('should have a baseUrlTenantCnamesSetPreferred function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesSetPreferred === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customCname', (done) => {
        try {
          a.baseUrlTenantCnamesSetPreferred(null, (data, error) => {
            try {
              const displayE = 'customCname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantCnamesSetPreferred', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesUiGet - errors', () => {
      it('should have a baseUrlTenantCnamesUiGet function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesUiGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantCnamesUnRegister - errors', () => {
      it('should have a baseUrlTenantCnamesUnRegister function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantCnamesUnRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customCname', (done) => {
        try {
          a.baseUrlTenantCnamesUnRegister(null, (data, error) => {
            try {
              const displayE = 'customCname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantCnamesUnRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigDeleteAdminSecurityQuestion - errors', () => {
      it('should have a baseUrlTenantConfigDeleteAdminSecurityQuestion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigDeleteAdminSecurityQuestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlTenantConfigDeleteAdminSecurityQuestion(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigDeleteAdminSecurityQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigDeleteAdminSecurityQuestion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigDeleteAdminSecurityQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigDeleteAdvancedConfig - errors', () => {
      it('should have a baseUrlTenantConfigDeleteAdvancedConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigDeleteAdvancedConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigDeleteAdvancedConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigDeleteAdvancedConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdminSecurityQuestion - errors', () => {
      it('should have a baseUrlTenantConfigGetAdminSecurityQuestion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetAdminSecurityQuestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlTenantConfigGetAdminSecurityQuestion(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigGetAdminSecurityQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdminSecurityQuestions - errors', () => {
      it('should have a baseUrlTenantConfigGetAdminSecurityQuestions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetAdminSecurityQuestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetAdvancedConfig - errors', () => {
      it('should have a baseUrlTenantConfigGetAdvancedConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetAdvancedConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetCustomerConfig - errors', () => {
      it('should have a baseUrlTenantConfigGetCustomerConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetCustomerConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetEditableMailTemplates - errors', () => {
      it('should have a baseUrlTenantConfigGetEditableMailTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetEditableMailTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetEditableMessageTemplate - errors', () => {
      it('should have a baseUrlTenantConfigGetEditableMessageTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetEditableMessageTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.baseUrlTenantConfigGetEditableMessageTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigGetEditableMessageTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateType', (done) => {
        try {
          a.baseUrlTenantConfigGetEditableMessageTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigGetEditableMessageTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetEditableMessageTemplates - errors', () => {
      it('should have a baseUrlTenantConfigGetEditableMessageTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetEditableMessageTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetGoogleKey - errors', () => {
      it('should have a baseUrlTenantConfigGetGoogleKey function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetGoogleKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetMobileConfig - errors', () => {
      it('should have a baseUrlTenantConfigGetMobileConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetMobileConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetSMTPConfig - errors', () => {
      it('should have a baseUrlTenantConfigGetSMTPConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetSMTPConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigGetTwilioConfig - errors', () => {
      it('should have a baseUrlTenantConfigGetTwilioConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigGetTwilioConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigResetPortalConfig - errors', () => {
      it('should have a baseUrlTenantConfigResetPortalConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigResetPortalConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSendTestMessageTemplate - errors', () => {
      it('should have a baseUrlTenantConfigSendTestMessageTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSendTestMessageTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templatePath', (done) => {
        try {
          a.baseUrlTenantConfigSendTestMessageTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templatePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSendTestMessageTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateType', (done) => {
        try {
          a.baseUrlTenantConfigSendTestMessageTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSendTestMessageTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetAdminSecurityQuestion - errors', () => {
      it('should have a baseUrlTenantConfigSetAdminSecurityQuestion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetAdminSecurityQuestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetAdminSecurityQuestion(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetAdminSecurityQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetAdvancedConfig - errors', () => {
      it('should have a baseUrlTenantConfigSetAdvancedConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetAdvancedConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetAdvancedConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetAdvancedConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetCustomerConfig - errors', () => {
      it('should have a baseUrlTenantConfigSetCustomerConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetCustomerConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetCustomerConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetCustomerConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetGoogleKey - errors', () => {
      it('should have a baseUrlTenantConfigSetGoogleKey function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetGoogleKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetGoogleKey(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetGoogleKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetMobileConfig - errors', () => {
      it('should have a baseUrlTenantConfigSetMobileConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetMobileConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetMobileConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetMobileConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetPasswordPersistance - errors', () => {
      it('should have a baseUrlTenantConfigSetPasswordPersistance function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetPasswordPersistance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.baseUrlTenantConfigSetPasswordPersistance(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetPasswordPersistance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetSMTPConfig - errors', () => {
      it('should have a baseUrlTenantConfigSetSMTPConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetSMTPConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetSMTPConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetSMTPConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigSetTwilioConfig - errors', () => {
      it('should have a baseUrlTenantConfigSetTwilioConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigSetTwilioConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigSetTwilioConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigSetTwilioConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigTestSMTPConfig - errors', () => {
      it('should have a baseUrlTenantConfigTestSMTPConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigTestSMTPConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigTestSMTPConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigTestSMTPConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlTenantConfigTestTwilioConfig - errors', () => {
      it('should have a baseUrlTenantConfigTestTwilioConfig function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlTenantConfigTestTwilioConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlTenantConfigTestTwilioConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlTenantConfigTestTwilioConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fAnswerRegistrationChallenge - errors', () => {
      it('should have a baseUrlU2fAnswerRegistrationChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fAnswerRegistrationChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rawRegisterResponse', (done) => {
        try {
          a.baseUrlU2fAnswerRegistrationChallenge(null, null, (data, error) => {
            try {
              const displayE = 'rawRegisterResponse is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fAnswerRegistrationChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlU2fAnswerRegistrationChallenge('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fAnswerRegistrationChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fDeleteU2fDevice - errors', () => {
      it('should have a baseUrlU2fDeleteU2fDevice function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fDeleteU2fDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyHandle', (done) => {
        try {
          a.baseUrlU2fDeleteU2fDevice(null, (data, error) => {
            try {
              const displayE = 'keyHandle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fDeleteU2fDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fDeleteU2fDevices - errors', () => {
      it('should have a baseUrlU2fDeleteU2fDevices function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fDeleteU2fDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlU2fDeleteU2fDevices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fDeleteU2fDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fFacets - errors', () => {
      it('should have a baseUrlU2fFacets function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fFacets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlU2fFacets(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fFacets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fGetRegistrationChallenge - errors', () => {
      it('should have a baseUrlU2fGetRegistrationChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fGetRegistrationChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userDefinedName', (done) => {
        try {
          a.baseUrlU2fGetRegistrationChallenge(null, null, null, (data, error) => {
            try {
              const displayE = 'userDefinedName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fGetRegistrationChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authenticatortype', (done) => {
        try {
          a.baseUrlU2fGetRegistrationChallenge('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'authenticatortype is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fGetRegistrationChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlU2fGetRegistrationChallenge('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fGetRegistrationChallenge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fGetU2fDevices - errors', () => {
      it('should have a baseUrlU2fGetU2fDevices function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fGetU2fDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlU2fGetU2fDevicesForUser - errors', () => {
      it('should have a baseUrlU2fGetU2fDevicesForUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlU2fGetU2fDevicesForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.baseUrlU2fGetU2fDevicesForUser(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlU2fGetU2fDevicesForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestGetAppByKey - errors', () => {
      it('should have a baseUrlUPRestGetAppByKey function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestGetAppByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appkey', (done) => {
        try {
          a.baseUrlUPRestGetAppByKey(null, null, (data, error) => {
            try {
              const displayE = 'appkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetAppByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUPRestGetAppByKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetAppByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestGetResultantAppsForUser - errors', () => {
      it('should have a baseUrlUPRestGetResultantAppsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestGetResultantAppsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userUuid', (done) => {
        try {
          a.baseUrlUPRestGetResultantAppsForUser(null, (data, error) => {
            try {
              const displayE = 'userUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetResultantAppsForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestGetTagsForApp - errors', () => {
      it('should have a baseUrlUPRestGetTagsForApp function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestGetTagsForApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appkey', (done) => {
        try {
          a.baseUrlUPRestGetTagsForApp(null, (data, error) => {
            try {
              const displayE = 'appkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetTagsForApp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestGetUPData - errors', () => {
      it('should have a baseUrlUPRestGetUPData function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestGetUPData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing force', (done) => {
        try {
          a.baseUrlUPRestGetUPData(null, null, (data, error) => {
            try {
              const displayE = 'force is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetUPData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.baseUrlUPRestGetUPData('fakeparam', null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestGetUPData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestSetUserCredsForApp - errors', () => {
      it('should have a baseUrlUPRestSetUserCredsForApp function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestSetUserCredsForApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUPRestSetUserCredsForApp(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestSetUserCredsForApp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUPRestUpsertTagsForApp - errors', () => {
      it('should have a baseUrlUPRestUpsertTagsForApp function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUPRestUpsertTagsForApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUPRestUpsertTagsForApp(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUPRestUpsertTagsForApp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtAnalyzeAdaptiveMfaRisk - errors', () => {
      it('should have a baseUrlUserMgmtAnalyzeAdaptiveMfaRisk function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtAnalyzeAdaptiveMfaRisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtCanEditUserAttributes - errors', () => {
      it('should have a baseUrlUserMgmtCanEditUserAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtCanEditUserAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtCanEditUserAttributes(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtCanEditUserAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlUserMgmtCanEditUserAttributes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtCanEditUserAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtChangeUserAttributes - errors', () => {
      it('should have a baseUrlUserMgmtChangeUserAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtChangeUserAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtChangeUserAttributes(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtChangeUserAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtChangeUserPassword - errors', () => {
      it('should have a baseUrlUserMgmtChangeUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtChangeUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtChangeUserPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtChangeUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtCheckUserProfileChallenge - errors', () => {
      it('should have a baseUrlUserMgmtCheckUserProfileChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtCheckUserProfileChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtDirectoryServiceQuery - errors', () => {
      it('should have a baseUrlUserMgmtDirectoryServiceQuery function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtDirectoryServiceQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtDirectoryServiceQuery(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtDirectoryServiceQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetCachedEntity - errors', () => {
      it('should have a baseUrlUserMgmtGetCachedEntity function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetCachedEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuidOrName', (done) => {
        try {
          a.baseUrlUserMgmtGetCachedEntity(null, (data, error) => {
            try {
              const displayE = 'uuidOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetCachedEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetCachedUser - errors', () => {
      it('should have a baseUrlUserMgmtGetCachedUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetCachedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuidOrName', (done) => {
        try {
          a.baseUrlUserMgmtGetCachedUser(null, (data, error) => {
            try {
              const displayE = 'uuidOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetCachedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetSecurityQuestions - errors', () => {
      it('should have a baseUrlUserMgmtGetSecurityQuestions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetSecurityQuestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlUserMgmtGetSecurityQuestions(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addAdminQuestions', (done) => {
        try {
          a.baseUrlUserMgmtGetSecurityQuestions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'addAdminQuestions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtGetSecurityQuestions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserAttributes - errors', () => {
      it('should have a baseUrlUserMgmtGetUserAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtGetUserAttributes(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlUserMgmtGetUserAttributes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserCertificateInfo - errors', () => {
      it('should have a baseUrlUserMgmtGetUserCertificateInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserCertificateInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtGetUserCertificateInfo(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserCertificateInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserHierarchy - errors', () => {
      it('should have a baseUrlUserMgmtGetUserHierarchy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserHierarchy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtGetUserHierarchy(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserHierarchy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserInfo - errors', () => {
      it('should have a baseUrlUserMgmtGetUserInfo function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtGetUserInfo(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserPicture - errors', () => {
      it('should have a baseUrlUserMgmtGetUserPicture function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserPicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtGetUserPicture(null, null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directoryServiceUuid', (done) => {
        try {
          a.baseUrlUserMgmtGetUserPicture('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directoryServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUserPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUserPreferences - errors', () => {
      it('should have a baseUrlUserMgmtGetUserPreferences function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUserPreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtGetUsersRolesAndAdministrativeRights - errors', () => {
      it('should have a baseUrlUserMgmtGetUsersRolesAndAdministrativeRights function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtGetUsersRolesAndAdministrativeRights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.baseUrlUserMgmtGetUsersRolesAndAdministrativeRights(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtGetUsersRolesAndAdministrativeRights', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtInviteUsers - errors', () => {
      it('should have a baseUrlUserMgmtInviteUsers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtInviteUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtInviteUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtInviteUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserCloudLocked - errors', () => {
      it('should have a baseUrlUserMgmtIsUserCloudLocked function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtIsUserCloudLocked === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtIsUserCloudLocked(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtIsUserCloudLocked', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserLockedOutByPolicy - errors', () => {
      it('should have a baseUrlUserMgmtIsUserLockedOutByPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtIsUserLockedOutByPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtIsUserLockedOutByPolicy(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtIsUserLockedOutByPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtIsUserSubjectToCloudLocks - errors', () => {
      it('should have a baseUrlUserMgmtIsUserSubjectToCloudLocks function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtIsUserSubjectToCloudLocks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtIsUserSubjectToCloudLocks(null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtIsUserSubjectToCloudLocks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtRemoveUser - errors', () => {
      it('should have a baseUrlUserMgmtRemoveUser function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtRemoveUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUser(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtRemoveUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtRemoveUserCertificate - errors', () => {
      it('should have a baseUrlUserMgmtRemoveUserCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtRemoveUserCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUserCertificate(null, null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtRemoveUserCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thumbprint', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUserCertificate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'thumbprint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtRemoveUserCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certType', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUserCertificate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'certType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtRemoveUserCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtRemoveUsers - errors', () => {
      it('should have a baseUrlUserMgmtRemoveUsers function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtRemoveUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtRemoveUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtRemoveUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtResetSecurityQuestions - errors', () => {
      it('should have a baseUrlUserMgmtResetSecurityQuestions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtResetSecurityQuestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtResetSecurityQuestions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtResetSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtResetUserPassword - errors', () => {
      it('should have a baseUrlUserMgmtResetUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtResetUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtResetUserPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtResetUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSendLoginEmail - errors', () => {
      it('should have a baseUrlUserMgmtSendLoginEmail function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSendLoginEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtSendLoginEmail(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSendLoginEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSendLoginEmails - errors', () => {
      it('should have a baseUrlUserMgmtSendLoginEmails function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSendLoginEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtSendLoginEmails(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSendLoginEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSendSmsInvite - errors', () => {
      it('should have a baseUrlUserMgmtSendSmsInvite function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSendSmsInvite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iD', (done) => {
        try {
          a.baseUrlUserMgmtSendSmsInvite(null, (data, error) => {
            try {
              const displayE = 'iD is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSendSmsInvite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSetCloudLock - errors', () => {
      it('should have a baseUrlUserMgmtSetCloudLock function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSetCloudLock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.baseUrlUserMgmtSetCloudLock(null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSetCloudLock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lockUser', (done) => {
        try {
          a.baseUrlUserMgmtSetCloudLock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'lockUser is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSetCloudLock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSetPhonePin - errors', () => {
      it('should have a baseUrlUserMgmtSetPhonePin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSetPhonePin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtSetPhonePin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSetPhonePin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtSetSecurityQuestion - errors', () => {
      it('should have a baseUrlUserMgmtSetSecurityQuestion function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtSetSecurityQuestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtSetSecurityQuestion(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtSetSecurityQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtUncacheUserPreferences - errors', () => {
      it('should have a baseUrlUserMgmtUncacheUserPreferences function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtUncacheUserPreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userUuidOrName', (done) => {
        try {
          a.baseUrlUserMgmtUncacheUserPreferences(null, (data, error) => {
            try {
              const displayE = 'userUuidOrName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtUncacheUserPreferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtUpdateSecurityQuestions - errors', () => {
      it('should have a baseUrlUserMgmtUpdateSecurityQuestions function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtUpdateSecurityQuestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtUpdateSecurityQuestions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtUpdateSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUserMgmtUpdateUserPreferences - errors', () => {
      it('should have a baseUrlUserMgmtUpdateUserPreferences function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUserMgmtUpdateUserPreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlUserMgmtUpdateUserPreferences(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUserMgmtUpdateUserPreferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlVfsGetFile - errors', () => {
      it('should have a baseUrlVfsGetFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlVfsGetFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlVfsGetFile(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlVfsGetFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlVfsGetFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlVfsGetFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlVfsGetFileLower - errors', () => {
      it('should have a baseUrlVfsGetFileLower function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlVfsGetFileLower === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlVfsGetFileLower(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlVfsGetFileLower', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoAuthenticateSession - errors', () => {
      it('should have a baseUrlZsoAuthenticateSession function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoAuthenticateSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.baseUrlZsoAuthenticateSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlZsoAuthenticateSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoCertLogin - errors', () => {
      it('should have a baseUrlZsoCertLogin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoCertLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing redirectUrl', (done) => {
        try {
          a.baseUrlZsoCertLogin(null, (data, error) => {
            try {
              const displayE = 'redirectUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlZsoCertLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoClearMacSafariZsoCookie - errors', () => {
      it('should have a baseUrlZsoClearMacSafariZsoCookie function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoClearMacSafariZsoCookie === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoIsMacSafariZsoCookieSet - errors', () => {
      it('should have a baseUrlZsoIsMacSafariZsoCookieSet function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoIsMacSafariZsoCookieSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoIsSessionAuthenticated - errors', () => {
      it('should have a baseUrlZsoIsSessionAuthenticated function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoIsSessionAuthenticated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.baseUrlZsoIsSessionAuthenticated(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlZsoIsSessionAuthenticated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlZsoSetMacSafariZsoCookie - errors', () => {
      it('should have a baseUrlZsoSetMacSafariZsoCookie function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlZsoSetMacSafariZsoCookie === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlHealthCheck - errors', () => {
      it('should have a baseUrlHealthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlHealthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlHomeGetLoginData - errors', () => {
      it('should have a baseUrlHomeGetLoginData function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlHomeGetLoginData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerid', (done) => {
        try {
          a.baseUrlHomeGetLoginData(null, null, null, (data, error) => {
            try {
              const displayE = 'customerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlHomeGetLoginData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing debug', (done) => {
        try {
          a.baseUrlHomeGetLoginData('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'debug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlHomeGetLoginData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlHomeGetLoginData('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlHomeGetLoginData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlLibOnlyVfsGetFile - errors', () => {
      it('should have a baseUrlLibOnlyVfsGetFile function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlLibOnlyVfsGetFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.baseUrlLibOnlyVfsGetFile(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLibOnlyVfsGetFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlLibOnlyVfsGetFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlLibOnlyVfsGetFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlOAuth2ManCreateClientToken - errors', () => {
      it('should have a baseUrlOAuth2ManCreateClientToken function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlOAuth2ManCreateClientToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.baseUrlOAuth2ManCreateClientToken(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlOAuth2ManCreateClientToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlPKILogin - errors', () => {
      it('should have a baseUrlPKILogin function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlPKILogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing redirUrl', (done) => {
        try {
          a.baseUrlPKILogin(null, (data, error) => {
            try {
              const displayE = 'redirUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlPKILogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#baseUrlUEvaluate - errors', () => {
      it('should have a baseUrlUEvaluate function', (done) => {
        try {
          assert.equal(true, typeof a.baseUrlUEvaluate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shortUrlKey', (done) => {
        try {
          a.baseUrlUEvaluate(null, (data, error) => {
            try {
              const displayE = 'shortUrlKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-baseUrlUEvaluate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAllowedReferrer - errors', () => {
      it('should have a addAllowedReferrer function', (done) => {
        try {
          assert.equal(true, typeof a.addAllowedReferrer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAllowedReferrer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAllowedReferrer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllowedReferrer - errors', () => {
      it('should have a getAllowedReferrer function', (done) => {
        try {
          assert.equal(true, typeof a.getAllowedReferrer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectories - errors', () => {
      it('should have a getDirectories function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDirectory - errors', () => {
      it('should have a createDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.createDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDirectory(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryDetails - errors', () => {
      it('should have a getDirectoryDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectoryDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectory - errors', () => {
      it('should have a deleteDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectoryMappingList - errors', () => {
      it('should have a getDirectoryMappingList function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectoryMappingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDirectoryMapping - errors', () => {
      it('should have a createDirectoryMapping function', (done) => {
        try {
          assert.equal(true, typeof a.createDirectoryMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDirectoryMapping(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createDirectoryMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMappingDetails - errors', () => {
      it('should have a getMappingDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getMappingDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDirectoryMapping - errors', () => {
      it('should have a editDirectoryMapping function', (done) => {
        try {
          assert.equal(true, typeof a.editDirectoryMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDirectoryMapping(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-editDirectoryMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectoryMapping - errors', () => {
      it('should have a deleteDirectoryMapping function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDirectoryMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reorderDirectoryMappings - errors', () => {
      it('should have a reorderDirectoryMappings function', (done) => {
        try {
          assert.equal(true, typeof a.reorderDirectoryMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.reorderDirectoryMappings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-reorderDirectoryMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitoraLiveSession - errors', () => {
      it('should have a monitoraLiveSession function', (done) => {
        try {
          assert.equal(true, typeof a.monitoraLiveSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resumeaSuspendedSession - errors', () => {
      it('should have a resumeaSuspendedSession function', (done) => {
        try {
          assert.equal(true, typeof a.resumeaSuspendedSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.resumeaSuspendedSession(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-resumeaSuspendedSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suspendanActiveSession - errors', () => {
      it('should have a suspendanActiveSession function', (done) => {
        try {
          assert.equal(true, typeof a.suspendanActiveSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.suspendanActiveSession(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-suspendanActiveSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#terminateanActiveSession - errors', () => {
      it('should have a terminateanActiveSession function', (done) => {
        try {
          assert.equal(true, typeof a.terminateanActiveSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.terminateanActiveSession(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-terminateanActiveSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordings - errors', () => {
      it('should have a getRecordings function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getRecordings(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getRecordings('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing safe', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'safe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTime', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fromTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toTime', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'toTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing activities', (done) => {
        try {
          a.getRecordings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'activities is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRecordings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingDetails - errors', () => {
      it('should have a getRecordingDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordingDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingActivities - errors', () => {
      it('should have a getRecordingActivities function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordingActivities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordingProperties - errors', () => {
      it('should have a getRecordingProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordingProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#playRecording - errors', () => {
      it('should have a playRecording function', (done) => {
        try {
          assert.equal(true, typeof a.playRecording === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessions - errors', () => {
      it('should have a getLiveSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getLiveSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getLiveSessions(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getLiveSessions('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing safe', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'safe is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTime', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'fromTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toTime', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'toTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing activities', (done) => {
        try {
          a.getLiveSessions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'activities is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getLiveSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionDetails - errors', () => {
      it('should have a getLiveSessionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getLiveSessionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionActivities - errors', () => {
      it('should have a getLiveSessionActivities function', (done) => {
        try {
          assert.equal(true, typeof a.getLiveSessionActivities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLiveSessionProperties - errors', () => {
      it('should have a getLiveSessionProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getLiveSessionProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnboardingRule - errors', () => {
      it('should have a getOnboardingRule function', (done) => {
        try {
          assert.equal(true, typeof a.getOnboardingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOnboardingRule - errors', () => {
      it('should have a addOnboardingRule function', (done) => {
        try {
          assert.equal(true, typeof a.addOnboardingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOnboardingRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addOnboardingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOnboardingRule - errors', () => {
      it('should have a updateOnboardingRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateOnboardingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOnboardingRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateOnboardingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateOnboardingRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateOnboardingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOnboardingRule - errors', () => {
      it('should have a deleteOnboardingRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOnboardingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteOnboardingRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteOnboardingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccountACL - errors', () => {
      it('should have a listAccountACL function', (done) => {
        try {
          assert.equal(true, typeof a.listAccountACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLAddress', (done) => {
        try {
          a.listAccountACL(null, null, null, (data, error) => {
            try {
              const displayE = 'aCLAddress, aCLUserName or aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-listAccountACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccountACL - errors', () => {
      it('should have a addAccountACL function', (done) => {
        try {
          assert.equal(true, typeof a.addAccountACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccountACL(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccountACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLAddress', (done) => {
        try {
          a.addAccountACL('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'aCLAddress, aCLUserName or aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addAccountACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountACL - errors', () => {
      it('should have a deleteAccountACL function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccountACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAccountACL(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteAccountACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aCLAddress', (done) => {
        try {
          a.deleteAccountACL('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'aCLAddress, aCLUserName or aCLPolicyID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteAccountACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTargetPlatforms - errors', () => {
      it('should have a getTargetPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getTargetPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateTargetPlatform - errors', () => {
      it('should have a activateTargetPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.activateTargetPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateTargetPlatform - errors', () => {
      it('should have a deactivateTargetPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateTargetPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#duplicateTargetPlatforms - errors', () => {
      it('should have a duplicateTargetPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.duplicateTargetPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.duplicateTargetPlatforms(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-duplicateTargetPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTargetPlatform - errors', () => {
      it('should have a deleteTargetPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTargetPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDependentPlatforms - errors', () => {
      it('should have a getDependentPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getDependentPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getDependentPlatforms(null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getDependentPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#duplicateDependentPlatforms - errors', () => {
      it('should have a duplicateDependentPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.duplicateDependentPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.duplicateDependentPlatforms(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-duplicateDependentPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDependentPlatform - errors', () => {
      it('should have a deleteDependentPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDependentPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupPlatforms - errors', () => {
      it('should have a getGroupPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getGroupPlatforms(null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getGroupPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateGroupPlatform - errors', () => {
      it('should have a activateGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.activateGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateGroupPlatform - errors', () => {
      it('should have a deactivateGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#duplicateGroupPlatforms - errors', () => {
      it('should have a duplicateGroupPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.duplicateGroupPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.duplicateGroupPlatforms(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-duplicateGroupPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupPlatform - errors', () => {
      it('should have a deleteGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRotationalGroupPlatforms - errors', () => {
      it('should have a getRotationalGroupPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getRotationalGroupPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getRotationalGroupPlatforms(null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getRotationalGroupPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateRotationalGroupPlatform - errors', () => {
      it('should have a activateRotationalGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.activateRotationalGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateRotationalGroupPlatform - errors', () => {
      it('should have a deactivateRotationalGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateRotationalGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#duplicateRotationalGroupPlatforms - errors', () => {
      it('should have a duplicateRotationalGroupPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.duplicateRotationalGroupPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.duplicateRotationalGroupPlatforms(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-duplicateRotationalGroupPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRotationalGroupPlatform - errors', () => {
      it('should have a deleteRotationalGroupPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRotationalGroupPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlatformDetails - errors', () => {
      it('should have a getPlatformDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getPlatformDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlatforms - errors', () => {
      it('should have a getPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing active', (done) => {
        try {
          a.getPlatforms(null, null, null, (data, error) => {
            try {
              const displayE = 'active is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platformType', (done) => {
        try {
          a.getPlatforms('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'platformType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platformName', (done) => {
        try {
          a.getPlatforms('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'platformName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportPlatform - errors', () => {
      it('should have a exportPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.exportPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPlatform - errors', () => {
      it('should have a importPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.importPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importPlatform(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-importPlatform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installPTA - errors', () => {
      it('should have a installPTA function', (done) => {
        try {
          assert.equal(true, typeof a.installPTA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serverEncryptionKey - errors', () => {
      it('should have a serverEncryptionKey function', (done) => {
        try {
          assert.equal(true, typeof a.serverEncryptionKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pTAServerAuthentication - errors', () => {
      it('should have a pTAServerAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.pTAServerAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityEvents - errors', () => {
      it('should have a getSecurityEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecuritySettings - errors', () => {
      it('should have a getSecuritySettings function', (done) => {
        try {
          assert.equal(true, typeof a.getSecuritySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRiskyCommandsRule - errors', () => {
      it('should have a addRiskyCommandsRule function', (done) => {
        try {
          assert.equal(true, typeof a.addRiskyCommandsRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRiskyCommandsRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addRiskyCommandsRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRiskyCommandsRule - errors', () => {
      it('should have a updateRiskyCommandsRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateRiskyCommandsRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRiskyCommandsRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateRiskyCommandsRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityRemediationSettings - errors', () => {
      it('should have a updateSecurityRemediationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityRemediationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityEvent - errors', () => {
      it('should have a updateSecurityEvent function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSecurityEvent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateSecurityEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTAReplicationStatus - errors', () => {
      it('should have a getPTAReplicationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getPTAReplicationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncomingRequestList - errors', () => {
      it('should have a getIncomingRequestList function', (done) => {
        try {
          assert.equal(true, typeof a.getIncomingRequestList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing onlywaiting', (done) => {
        try {
          a.getIncomingRequestList(null, null, (data, error) => {
            try {
              const displayE = 'onlywaiting is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getIncomingRequestList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expired', (done) => {
        try {
          a.getIncomingRequestList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expired is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getIncomingRequestList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsofaRequestforConfirmation - errors', () => {
      it('should have a getDetailsofaRequestforConfirmation function', (done) => {
        try {
          assert.equal(true, typeof a.getDetailsofaRequestforConfirmation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmRequest - errors', () => {
      it('should have a confirmRequest function', (done) => {
        try {
          assert.equal(true, typeof a.confirmRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.confirmRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-confirmRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectRequest - errors', () => {
      it('should have a rejectRequest function', (done) => {
        try {
          assert.equal(true, typeof a.rejectRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rejectRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-rejectRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMyRequests - errors', () => {
      it('should have a getMyRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getMyRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing onlywaiting', (done) => {
        try {
          a.getMyRequests(null, null, (data, error) => {
            try {
              const displayE = 'onlywaiting is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getMyRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing expired', (done) => {
        try {
          a.getMyRequests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'expired is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getMyRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createaRequest - errors', () => {
      it('should have a createaRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createaRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createaRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-createaRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsofMyRequests - errors', () => {
      it('should have a getDetailsofMyRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getDetailsofMyRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMyRequest - errors', () => {
      it('should have a deleteMyRequest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMyRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteMyRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-deleteMyRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logo - errors', () => {
      it('should have a logo function', (done) => {
        try {
          assert.equal(true, typeof a.logo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.logo(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-logo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#server - errors', () => {
      it('should have a server function', (done) => {
        try {
          assert.equal(true, typeof a.server === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verify - errors', () => {
      it('should have a verify function', (done) => {
        try {
          assert.equal(true, typeof a.verify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectionComponents - errors', () => {
      it('should have a getAllConnectionComponents function', (done) => {
        try {
          assert.equal(true, typeof a.getAllConnectionComponents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPSMServers - errors', () => {
      it('should have a getAllPSMServers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPSMServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionManagementPolicyofPlatform - errors', () => {
      it('should have a getSessionManagementPolicyofPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionManagementPolicyofPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importConnectionComponent - errors', () => {
      it('should have a importConnectionComponent function', (done) => {
        try {
          assert.equal(true, typeof a.importConnectionComponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importConnectionComponent(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-importConnectionComponent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSessionManagementPolicyofPlatform - errors', () => {
      it('should have a updateSessionManagementPolicyofPlatform function', (done) => {
        try {
          assert.equal(true, typeof a.updateSessionManagementPolicyofPlatform === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSessionManagementPolicyofPlatform(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-updateSessionManagementPolicyofPlatform', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfiguration - errors', () => {
      it('should have a getConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTASystemHealth - errors', () => {
      it('should have a getPTASystemHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getPTASystemHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing match', (done) => {
        try {
          a.getPTASystemHealth(null, (data, error) => {
            try {
              const displayE = 'match is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-getPTASystemHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemDetails - errors', () => {
      it('should have a systemDetails function', (done) => {
        try {
          assert.equal(true, typeof a.systemDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemSummary - errors', () => {
      it('should have a systemSummary function', (done) => {
        try {
          assert.equal(true, typeof a.systemSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicSSHKey - errors', () => {
      it('should have a getPublicSSHKey function', (done) => {
        try {
          assert.equal(true, typeof a.getPublicSSHKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addaPublicSSHKey - errors', () => {
      it('should have a addaPublicSSHKey function', (done) => {
        try {
          assert.equal(true, typeof a.addaPublicSSHKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addaPublicSSHKey(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-addaPublicSSHKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePublicSSHKey - errors', () => {
      it('should have a deletePublicSSHKey function', (done) => {
        try {
          assert.equal(true, typeof a.deletePublicSSHKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateanMFACachingSSHKey - errors', () => {
      it('should have a generateanMFACachingSSHKey function', (done) => {
        try {
          assert.equal(true, typeof a.generateanMFACachingSSHKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.generateanMFACachingSSHKey(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-generateanMFACachingSSHKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateanMFACachingSSHKeyforAnotherUser - errors', () => {
      it('should have a generateanMFACachingSSHKeyforAnotherUser function', (done) => {
        try {
          assert.equal(true, typeof a.generateanMFACachingSSHKeyforAnotherUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.generateanMFACachingSSHKeyforAnotherUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cyberark-adapter-generateanMFACachingSSHKeyforAnotherUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteanMFACachingSSHKeyforAnotherUser - errors', () => {
      it('should have a deleteanMFACachingSSHKeyforAnotherUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteanMFACachingSSHKeyforAnotherUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllMFACachingSSHKeys - errors', () => {
      it('should have a deleteAllMFACachingSSHKeys function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllMFACachingSSHKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteanMFACachingSSHkey - errors', () => {
      it('should have a deleteanMFACachingSSHkey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteanMFACachingSSHkey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
