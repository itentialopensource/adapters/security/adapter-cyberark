
## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cyberark!1

---

## 0.1.1 [08-02-2021]

- Initial Commit

See commit 542dd2a

---
